#!/bin/sh
#PBS -q qfree
#PBS -l select=1:ncpus=24:accelerator=True:naccelerators=1
#PBS -l walltime=60:00
#PBS -A OPEN-6-11




module load intel

cd $PBS_O_WORKDIR

make

#export SINK_LD_LIBRARY_PATH=$MIC_LD_LIBRARY_PATH
export SINK_LD_LIBRARY_PATH=/apps/all/icc/2016.1.150-GCC-4.9.3-2.25/lib/mic


export KMP_AFFINITY=granularity=core,compact

for threads in 1 2 4 8 16 24
do
export OMP_NUM_THREADS=$threads

export KMP_AFFINITY=granularity=fine,compact
./stream >> results/stream_cpu_compact.txt ;
./stream >> results/stream_cpu_compact.txt ;

export KMP_AFFINITY=granularity=fine,scatter
./stream >> results/stream_cpu_scatter.txt ;
./stream >> results/stream_cpu_scatter.txt ;
done

for threads in 1 30 60 120 180 240
do
micnativeloadex ./stream-mic -e "OMP_NUM_THREADS=$threads" -e "KMP_AFFINITY=granularity=fine,compact" >> results/stream_mic_compact.txt ;
micnativeloadex ./stream-mic -e "OMP_NUM_THREADS=$threads" -e "KMP_AFFINITY=granularity=fine,compact" >> results/stream_mic_compact.txt ;
micnativeloadex ./stream-mic -e "OMP_NUM_THREADS=$threads" -e "KMP_AFFINITY=granularity=fine,scatter" >> results/stream_mic_scatter.txt ;
micnativeloadex ./stream-mic -e "OMP_NUM_THREADS=$threads" -e "KMP_AFFINITY=granularity=fine,scatter" >> results/stream_mic_scatter.txt ;
done

exit
