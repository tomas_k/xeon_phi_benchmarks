for(step = 0; step < steps; ++step)
{
    for(i = 0; i < N; ++i)
    {
        F = 0;
        for(j = 0; j < N; ++j)
        {
            if(i != j)
                F += calculate_force(particle[i], particle[j]);
        }
        ACC = calculate_acceleration(particle[i], F);
        update_velocity(particle[i], ACC);
        update_position(particle[i]);
    }
}