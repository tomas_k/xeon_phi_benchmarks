#pragma omp parallel for shared(A,B,C,k) private(i)
for (i=0; i<N; i++)
    A[i] = K*B[i] + C[i];
