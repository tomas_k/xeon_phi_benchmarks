for(step = 0; step < steps; ++step)
{
    buildOctree(octree,particles);
    sumarizeSubtrees(octree);
    
    computeForces(octree,particles);
    
    advance(particles,timestep);
}