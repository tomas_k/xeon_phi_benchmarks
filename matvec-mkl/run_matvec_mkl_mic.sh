#!/bin/sh
#PBS -q qfree
#PBS -l select=1:ncpus=24:accelerator=True:naccelerators=2 
#PBS -l walltime=30:00
#PBS -A OPEN-6-11


module load intel
cd $PBS_O_WORKDIR
make

export SINK_LD_LIBRARY_PATH=$MIC_LD_LIBRARY_PATH


OUTPUT="results_mic_scatter.txt"
ITERATIONS=5000


for threads in 60 120 180 240
do
for msize in 64 1024 2048 4096 8192 16384 32768
do
micnativeloadex ./matvec-mkl-mic -a "$msize $msize $ITERATIONS" -e "KMP_AFFINITY=graunlarity=fine,scatter" -e "OMP_NUM_THREADS=$threads" | tee -a $OUTPUT
done
done
