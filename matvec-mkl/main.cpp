
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <omp.h>

#include <mkl.h>

float** mat_alloc(int rows, int cols, int align);
void mat_free(int rows, int cols, float** mat);
void mat_init(int row, int col, float off, float* a);
void vec_init(int length, float off, float a[]);
double vec_sum(int length, float vec[]);


int main(int argc, char** argv)
{
    int rows = 0;
    int cols = 0;
    int runs = 0;
    
    if(argc != 4)
    {
        printf("Usage: matvec <rows> <cols> <runs>\n");
        return -1;
    }
    
    rows = strtol(argv[1],NULL,10);
    cols = strtol(argv[2],NULL,10);
    runs = strtol(argv[3],NULL,10);
    
    //printf("Running with rows=%d, cols=%d, runs=%d\n",rows,cols,runs);
    
    
    int i;

    float* a = static_cast<float*>(_mm_malloc(rows*cols*sizeof(float),ALIGNMENT));
    float* b = static_cast<float*>(_mm_malloc(cols*sizeof(float),ALIGNMENT));
    float* c = static_cast<float*>(_mm_malloc(rows*sizeof(float),ALIGNMENT));

    // initialize matrix & vector
    mat_init(rows, cols, 1.0, a);
    vec_init(cols, 3.0, b);
    memset(c,0,rows*sizeof(float));
    
    // warm up
    cblas_sgemv(CblasRowMajor,CblasNoTrans,rows,cols,1.0f,a,rows,b,1,1.0f,c,1);

    double start = omp_get_wtime();
    for (i = 0; i < runs; i++)
    {
        cblas_sgemv(CblasRowMajor,CblasNoTrans,rows,cols,1.0f,a,cols,b,1,0.0f,c,1);
    }
    double end = omp_get_wtime();
    

    double diff = end - start;
    //printf("time: %lf\n", diff);
    //printf("MFLOPS: %.2lf\n",((rows*cols)*2*double(runs)) / diff / 1000000);
    
    int threads=omp_get_max_threads();

    printf("%d %d %d %lf %.0lf\n",rows,threads,runs,diff,((rows*cols)*2*double(runs)) / diff / 1000000);
    
    
    _mm_free(a);
    _mm_free(b);
    _mm_free(c);

    return 0;
}


float** mat_alloc(int rows, int cols, int align)
{
    float** mat = static_cast<float**>(_mm_malloc(rows*sizeof(float*),align));
    for(int i=0; i<rows; ++i)
    {
        mat[i] = static_cast<float*>(_mm_malloc(cols*sizeof(float),align));
    }
    
    return mat;
}

void mat_free(int rows, int cols, float** mat)
{
    for(int i=0; i<rows; ++i)
    {
        _mm_free(mat[i]);
    }
    
    _mm_free(mat);
}

void mat_init(int row, int col, float off, float* a)
{
#pragma omp parallel for
    for (int i = 0; i < row; i++)
        for (int j = 0; j < col; j++)
            a[i*row + j] = (i+1);
}

void vec_init(int length, float off, float a[])
{
    int i;

    for (i = 0; i < length; i++)
        a[i] = (i+1);
}

double
vec_sum(int length, float vec[])
{
    int i;
    double sum = 0.0;

    for (i = 0; i < length; i++)
        sum += vec[i];

    return sum;
}


