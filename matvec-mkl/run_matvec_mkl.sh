#!/bin/sh
#PBS -q qfree
#PBS -l select=1:ncpus=24:accelerator=True:naccelerators=2 
#PBS -l walltime=30:00
#PBS -A OPEN-6-11


module load intel
cd $PBS_O_WORKDIR
make

export OMP_NUM_THREADS=24
export KMP_AFFINITY=granularity=core,compact
export SINK_LD_LIBRARY_PATH=$MIC_LD_LIBRARY_PATH


OUTPUT="results_cpu.txt"
ITERATIONS=5000

//for threads in 1 2 4 8 12 24
//do
//export OMP_NUM_THREADS=$threads
for msize in 64 256 1024 2048 4096 8192 
do
./matvec-mkl $msize $msize $ITERATIONS | tee -a $OUTPUT
done
//done
