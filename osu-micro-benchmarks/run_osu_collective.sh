#!/bin/bash
#PBS -A OPEN-6-11
#PBS -l select=3:ncpus=24:accelerator=True:naccelerators=2 
#PBS -l walltime=60:00


BENCHMARK_NAME="scatter"

module load intel
. scripts/setup_mpi_env.sh

NODE1=`sed -n '1p' < /lscratch/$PBS_JOBID/nodefile-cn-sn`
NODE2=`sed -n '2p' < /lscratch/$PBS_JOBID/nodefile-cn-sn`
NODE3=`sed -n '3p' < /lscratch/$PBS_JOBID/nodefile-cn-sn`

RESULTSDIR="results/collective/"$BENCHMARK_NAME"/"


cd $PBS_O_WORKDIR



#export MPI_PIN_PROCESSOR_LIST=all:map=bunch
export MPI_PIN_PROCESSOR_LIST=all:map=scatter
#export MPI_PIN_PROCESSOR_LIST=all:map=compact


mkdir -p $RESULTSDIR


BENCHMARKS=(osu_reduce osu_scatter osu_bcast osu_barrier osu_allgather osu_allreduce osu_alltoall)

for bench in ${BENCHMARKS[@]}
do
    # cpu only
    mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 1 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-cpu-3.txt"
    mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 2 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-cpu-6.txt"
    mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 4 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-cpu-12.txt"
    mpirun -hosts $NODE1,$NODE2 -ppn 1 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-2node-cpu-2.txt"
    mpirun -hosts $NODE1,$NODE2 -ppn 2 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-2node-cpu-4.txt"
    mpirun -hosts $NODE1,$NODE2 -ppn 4 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-2node-cpu-8.txt"
    mpirun -hosts $NODE1 -ppn 2 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-1node-cpu-2.txt"
    mpirun -hosts $NODE1 -ppn 4 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-1node-cpu-4.txt"
    
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mic-sn -ppn 1 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mic-6.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mic-sn -ppn 2 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mic-12.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mic-sn -ppn 4 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mic-24.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mic-sn -ppn 8 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mic-48.txt"
    
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -host $NODE1"-mic0" -n 4 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-1node-mic-4.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -host $NODE1"-mic0" -n 8 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-1node-mic-8.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -host $NODE1"-mic0" -n 16 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-1node-mic-16.txt"
    
    
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mix-sn -ppn 1 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mix-ppn1.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mix-sn -ppn 2 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mix-ppn2.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mix-sn -ppn 4 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mix-ppn4.txt"
    mpirun -genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH -hostfile /lscratch/$PBS_JOBID/nodefile-mix-sn -ppn 8 ./bin/collective/$bench >> ${RESULTSDIR}${bench}"-3node-mix-ppn8.txt"

done




