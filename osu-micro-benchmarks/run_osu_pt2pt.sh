#!/bin/bash
#PBS -A OPEN-6-11
#PBS -l select=2:ncpus=24:accelerator=True:naccelerators=2 
#PBS -l walltime=15:00


BENCHMARK_NAME="compact"

module load intel
. scripts/setup_mpi_env.sh

NODE1=`sed -n '1p' < /lscratch/$PBS_JOBID/nodefile-mix-sn`
NODE2=`sed -n '2p' < /lscratch/$PBS_JOBID/nodefile-mix-sn`

RESULTSDIR="results/"$BENCHMARK_NAME"/"


cd $PBS_O_WORKDIR



#export MPI_PIN_PROCESSOR_LIST=all:map=bunch
#export MPI_PIN_PROCESSOR_LIST=all:map=scatter
export MPI_PIN_PROCESSOR_LIST=all:map=compact


mkdir -p $RESULTSDIR

#PBS_JOBID
echo "2 nodes cpu"
mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 1 ./bin/pt2pt/osu_bw >> $RESULTSDIR"osu_bw_cpu_2node.txt"
mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 1 ./bin/pt2pt/osu_bibw >> $RESULTSDIR"osu_bibw_cpu_2node.txt"
mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 1 ./bin/pt2pt/osu_latency >> $RESULTSDIR"osu_latency_cpu_2node.txt"
mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 1 ./bin/pt2pt/osu_latency_mt >> $RESULTSDIR"osu_latency_mt_cpu_2node.txt"
mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 1 ./bin/pt2pt/osu_multi_lat >> $RESULTSDIR"osu_multi_lat_cpu_2node.txt"

echo "1 node cpu"
mpirun -host $NODE1 -n 2 ./bin/pt2pt/osu_bw >> $RESULTSDIR"osu_bw_cpu_1node.txt"
mpirun -host $NODE1 -n 2 ./bin/pt2pt/osu_bibw >> $RESULTSDIR"osu_bibw_cpu_1node.txt"
mpirun -host $NODE1 -n 2 ./bin/pt2pt/osu_latency >> $RESULTSDIR"osu_latency_cpu_1node.txt"
mpirun -host $NODE1 -n 2 ./bin/pt2pt/osu_latency_mt >> $RESULTSDIR"osu_latency_mt_cpu_1node.txt"
mpirun -host $NODE1 -n 2 ./bin/pt2pt/osu_multi_lat >> $RESULTSDIR"osu_multi_lat_cpu_1node.txt"

echo "1 mic 1 node"
mpirun -host $NODE1"-mic0" -n 2 ./bin/pt2pt/osu_bw >> $RESULTSDIR"osu_bw_mic_1node_1mic.txt"
mpirun -host $NODE1"-mic0" -n 2 ./bin/pt2pt/osu_bibw >> $RESULTSDIR"osu_bibw_mic_1node_1mic.txt"
mpirun -host $NODE1"-mic0" -n 2 ./bin/pt2pt/osu_latency >> $RESULTSDIR"osu_latency_mic_1node_1mic.txt"
mpirun -host $NODE1"-mic0" -n 2 ./bin/pt2pt/osu_latency_mt >> $RESULTSDIR"osu_latency_mt_mic_1node_1mic.txt"
mpirun -host $NODE1"-mic0" -n 2 ./bin/pt2pt/osu_multi_lat >> $RESULTSDIR"osu_multi_lat_mic_1node_1mic.txt"

echo "2 mic 2 nodes"
mpirun -hosts $NODE1"-mic0",$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_bw >> $RESULTSDIR"osu_bw_mic_2node.txt"
mpirun -hosts $NODE1"-mic0",$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_bibw >> $RESULTSDIR"osu_bibw_mic_2node.txt"
mpirun -hosts $NODE1"-mic0",$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_latency >> $RESULTSDIR"osu_latency_mic_2node.txt"
mpirun -hosts $NODE1"-mic0",$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_latency_mt >> $RESULTSDIR"osu_latency_mt_mic_2node.txt"
mpirun -hosts $NODE1"-mic0",$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_multi_lat >> $RESULTSDIR"osu_multi_lat_mic_2node.txt"

echo "2 mic 1 node"
mpirun -hosts $NODE1"-mic0",$NODE1"-mic1" -ppn 1 ./bin/pt2pt/osu_bw >> $RESULTSDIR"osu_bw_mic_1node_2mic.txt"
mpirun -hosts $NODE1"-mic0",$NODE1"-mic1" -ppn 1 ./bin/pt2pt/osu_bibw >> $RESULTSDIR"osu_bibw_mic_1node_2mic.txt"
mpirun -hosts $NODE1"-mic0",$NODE1"-mic1" -ppn 1 ./bin/pt2pt/osu_latency >> $RESULTSDIR"osu_latency_mic_1node_2mic.txt"
mpirun -hosts $NODE1"-mic0",$NODE1"-mic1" -ppn 1 ./bin/pt2pt/osu_latency_mt >> $RESULTSDIR"osu_latency_mt_mic_1node_2mic.txt"
mpirun -hosts $NODE1"-mic0",$NODE1"-mic1" -ppn 1 ./bin/pt2pt/osu_multi_lat >> $RESULTSDIR"osu_multi_lat_mic_1node_2mic.txt"

echo "1 cpu 1 mic 1 node"
mpirun -hosts $NODE1,$NODE1"-mic0" -ppn 1 ./bin/pt2pt/osu_bw >> $RESULTSDIR"osu_bw_cpu-mic_1node.txt"
mpirun -hosts $NODE1,$NODE1"-mic0" -ppn 1 ./bin/pt2pt/osu_bibw >> $RESULTSDIR"osu_bibw_cpu-mic_1node.txt"
mpirun -hosts $NODE1,$NODE1"-mic0" -ppn 1 ./bin/pt2pt/osu_latency >> $RESULTSDIR"osu_latency_cpu-mic_1node.txt"
mpirun -hosts $NODE1,$NODE1"-mic0" -ppn 1 ./bin/pt2pt/osu_latency_mt >> $RESULTSDIR"osu_latency_mt_cpu-mic_1node.txt"
mpirun -hosts $NODE1,$NODE1"-mic0" -ppn 1 ./bin/pt2pt/osu_multi_lat >> $RESULTSDIR"osu_multi_lat_cpu-mic_1node.txt"

echo "1 cpu 1 mic 2 nodex"
mpirun -hosts $NODE1,$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_bw >> $RESULTSDIR"osu_bw_cpu-mic_2node.txt"
mpirun -hosts $NODE1,$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_bibw >> $RESULTSDIR"osu_bibw_cpu-mic_2node.txt"
mpirun -hosts $NODE1,$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_latency >> $RESULTSDIR"osu_latency_cpu-mic_2node.txt"
mpirun -hosts $NODE1,$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_latency_mt >> $RESULTSDIR"osu_latency_mt_cpu-mic_2node.txt"
mpirun -hosts $NODE1,$NODE2"-mic0" -ppn 1 ./bin/pt2pt/osu_multi_lat >> $RESULTSDIR"osu_multi_lat_cpu-mic_2node.txt"
