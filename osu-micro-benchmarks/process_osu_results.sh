#!/bin/sh

WDIR="x"

if [ -z "$1" ]; then WDIR="./"; else WDIR=$1; fi

FILES=$WDIR"*"

for f in $FILES
do
    echo "Processing $f"
    printf "`basename $f` " >> $WDIR"/results.txt"
    awk ' /^[0-9]/  { printf $2" " }; END {printf "\n" }' $f >> $WDIR"/results.txt"

done
