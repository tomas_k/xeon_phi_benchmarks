#!/usr/bin/gnuplot
# /apps/tools/gnuplot/4.6.5/bin/gnuplot

reset
set terminal svg

set ylabel "MFLOPs"
set xlabel "threads"

set title "Matvec scaling"
set grid

set output "plot_fc.svg"

plot for [IDX=0:8] "results_fc.txt" i IDX u 1:3:xtic(1) w lines title columnheader(1)

