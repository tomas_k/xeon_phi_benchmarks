
#include "matvec.h"

//#include "advisor-annotate.h" 

void mat_vec_mul(int rows, int cols, float** restrict a, float* restrict b, float* restrict c)
{
    int i, j;

    __assume_aligned(a,ALIGNMENT);
    __assume_aligned(b,ALIGNMENT);
    __assume_aligned(c,ALIGNMENT);
    
    __assume(rows%ALIGNMENT==0);
    __assume(cols%ALIGNMENT==0);

#pragma omp parallel for shared(a,b,c) private(i,j) schedule(static)
    for (i = 0; i < rows; i++)
    {
	float* row = a[i];
	__assume_aligned(row,ALIGNMENT);
        float ci = 0.0f;
#pragma omp simd reduction(+:ci)
        for (j = 0; j < cols; j++)
        {
            ci += row[j] * b[j];
        }
        c[i] = ci;
    }
}

