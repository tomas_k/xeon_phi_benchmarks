#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>

#include <omp.h>

#include "matvec.h"

#ifndef WITHOUT_PAPI
#include "papi_cntr.h"
#endif


float** mat_alloc(int rows, int cols, int align)
{
    float** mat = static_cast<float**>(_mm_malloc(rows*sizeof(float*),align));
    for(int i=0; i<rows; ++i)
    {
        mat[i] = static_cast<float*>(_mm_malloc(cols*sizeof(float),align));
    }
    
    return mat;
}

void mat_free(int rows, int cols, float** mat)
{
    for(int i=0; i<rows; ++i)
    {
        _mm_free(mat[i]);
    }
    
    _mm_free(mat);
}

void mat_init(int row, int col, float off, float** a)
{
#pragma omp parallel for
    for (int i = 0; i < row; i++)
        for (int j = 0; j < col; j++)
            a[i][j] = fmod(i * j + off, 10.0);
}

void vec_init(int length, float off, float a[])
{
    int i;

    for (i = 0; i < length; i++)
        a[i] = fmod(i + off, 10.0);
}

double
vec_sum(int length, float vec[])
{
    int i;
    double sum = 0.0;

    for (i = 0; i < length; i++)
        sum += vec[i];

    return sum;
}

int main(int argc, char** argv)
{
    int rows = 0;
    int cols = 0;
    int runs = 0;
    
    if(argc != 4)
    {
        printf("Usage: matvec <rows> <cols> <runs>\n");
        return -1;
    }
    
    rows = strtol(argv[1],NULL,10);
    cols = strtol(argv[2],NULL,10);
    runs = strtol(argv[3],NULL,10);
    
    //printf("Running with rows=%d, cols=%d, runs=%d\n",rows,cols,runs);
    
    
    int i;
#ifndef WITHOUT_PAPI
    PapiCounterList papi_routines;
    papi_routines.AddRoutine("mat_vec_mul");
#endif

    float** a = mat_alloc(rows,cols,ALIGNMENT);
    float* b = static_cast<float*>(_mm_malloc(cols*sizeof(float),ALIGNMENT));
    float* c = static_cast<float*>(_mm_malloc(rows*sizeof(float),ALIGNMENT));

    // initialize matrix & vector
    mat_init(rows, cols, 1.0, a);
    vec_init(cols, 3.0, b);
    memset(c,0,rows*sizeof(float));

    mat_vec_mul(rows, cols, a, b, c);
    
// do the measurement
#ifndef WITHOUT_PAPI
    papi_routines["mat_vec_mul"].Start();
#endif

    double start = omp_get_wtime();
    for (i = 0; i < runs; i++)
        mat_vec_mul(rows, cols, a, b, c);
    double end = omp_get_wtime();

#ifndef WITHOUT_PAPI
    papi_routines["mat_vec_mul"].Stop();
    
    papi_routines.PrintScreen();
#else
    
    
#endif

    double diff = end - start;
    //printf("time: %lf\n", diff);
    //printf("MFLOPS: %.2lf\n",((rows*cols)*2*double(runs)) / diff / 1000000);
    
    int threads=omp_get_max_threads();

    printf("%d %d %lf %.0lf\n",rows,threads,diff,((rows*cols)*2*double(runs)) / diff / 1000000);
    
    
    //printf("control sum: %f\n", vec_sum(rows, c));
    //printf("\n");
    
    mat_free(rows,cols,a);
    _mm_free(b);
    _mm_free(c);

    return 0;
}
