#ifndef OPT_H
#define OPT_H

#ifdef __GNUC__
#ifdef __INTEL_COMPILER
#define ASSUME_ALIGNED(what,how)  __assume_aligned(what,how)
#else
#define ASSUME_ALIGNED(what,how)  __builtin_assume_aligned(what,how)
#endif

#else

#endif

#endif // OPT_H

