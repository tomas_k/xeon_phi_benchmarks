#ifndef __MATVEC_H__
#define __MATVEC_H__

void mat_vec_mul(int rows, int cols, float** restrict a, float* restrict b, float* restrict c);

#endif /* __MATVEC_H__ */
