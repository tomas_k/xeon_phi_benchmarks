#!/bin/bash
#PBS -A OPEN-6-11
#PBS -N BH_1
#PBS -q qfree
#PBS -l walltime=00:40:00
#PBS -l select=1:ncpus=24:accelerator=True:naccelerators=2

module load intel

export I_MPI_MIC=enable
export I_MPI_PIN_DOMAIN=omp
export I_MPI_MIC_POSTFIX=_mic
export I_MPI_FABRICS=shm:dapl
export I_MPI_DAPL_PROVIDER_LIST=ofa-v2-mlx4_0-1u,ofa-v2-scif0,ofa-v2-mcm-1

export KMP_AFFINITY=granularity=fine,compact


cd "$PBS_O_WORKDIR"

jobid=$PBS_JOBID




for dataset in 5 1 6 2 3 7 4 8
do
	export OMP_NUM_THREADS=24
	mpirun -hostfile /lscratch/$PBS_JOBID/nodefile-cn-sn -ppn 1 ../barnes_hut-openmp-mpi/nbody 50 0.5 6 "../dataset/mix$dataset" "idc" 1 0 | tee -a results_bh_cpu_1.txt
done


#-genv LD_LIBRARY_PATH $MIC_LD_LIBRARY_PATH
