#!/bin/sh

for i in 1 2 4 8; do
qsub naive_cpu_${i}.sh;
qsub naive_cpu_${i}_ppn2.sh;
qsub bh_cpu_${i}.sh;
qsub bh_cpu_${i}_ppn2.sh;
done

for i in 2 4 8; do
qsub naive_mic_${i}.sh;
qsub naive_mic_${i}_ppn2.sh;
qsub bh_mic_${i}.sh;
qsub bh_mic_${i}_ppn2.sh;

qsub naive_mix_${i}.sh;
qsub naive_mix_${i}_ppn2.sh;
qsub bh_mix_${i}.sh;
qsub bh_mix_${i}_ppn2.sh;
done


