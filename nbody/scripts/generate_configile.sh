#!/bin/sh

input_file=$1
procs_per_cpu=$2
procs_per_mic=$3
output_file=$4
cpu_threads=$5
mic_threads=$6
executable=$7
params=$8

rm -rf $output_file

while read line
do
	echo "-host $line -env OMP_NUM_THREADS $cpu_threads -n $procs_per_cpu $executable $params" >> $output_file
	echo "-host $line-mic0 -env OMP_NUM_THREADS $mic_threads -n $procs_per_mic ${executable} $params" >> $output_file
	echo "-host $line-mic1 -env OMP_NUM_THREADS $mic_threads -n $procs_per_mic ${executable} $params" >> $output_file
done < $input_file

