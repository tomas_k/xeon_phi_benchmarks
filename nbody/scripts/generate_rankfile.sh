#!/bin/sh

input_file=$1
procs_per_cpu=$2
procs_per_mic=$3
output_file=$4

while read line
do
    if [[ $line == *"mic"* ]]
    then
		if [[ "$procs_per_mic" -gt "0" ]]
		then
        	echo "$line:$procs_per_mic" >> $output_file
		fi
    else
        if [[ "$procs_per_cpu" -gt "0" ]]
		then
        	echo "$line:$procs_per_cpu" >> $output_file
		fi
    fi
done < $input_file

