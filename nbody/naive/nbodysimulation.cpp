#include "nbodysimulation.h"

#include "stopwatch.h"

#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <limits>
#include <iomanip>
#include <cassert>

#ifndef __MIC__
#include <random>
#include <functional>
#endif

#include <cmath>

#include <omp.h>

#include <inttypes.h>

#define GRAVITATIONAL_CONSTANT 1.0f

const char* delim = "\t";

using namespace std;

#include <mpi.h>

void NBody::resizeParticleStorage(size_t size)
{
    for(avector<float>& av : particles)
    {
        av.resize(size);
    }
}

NBody::NBody() : stopwatch(TIMERS_NAMES_MAX)
{
    num_particles = 0;

    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&world_size);

    std::ios_base::sync_with_stdio(false);
}

// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch31.html
void NBody::runSeq(const unsigned int steps, const float dtime)
{
    if( num_particles == 0)
        return;

    if(world_size != 1)
        return;

    float* mass = particles[WEIGHT].data();
    float* posx = particles[POS_X].data();
    float* posy = particles[POS_Y].data();
    float* posz = particles[POS_Z].data();
    float* velx = particles[VEL_X].data();
    float* vely = particles[VEL_Y].data();
    float* velz = particles[VEL_Z].data();

    const float eps2 = 0.001;
    const float g = GRAVITATIONAL_CONSTANT;

    ASSUME_ALIGNED(mass,ALIGNMENT);
    ASSUME_ALIGNED(posx,ALIGNMENT);
    ASSUME_ALIGNED(posy,ALIGNMENT);
    ASSUME_ALIGNED(posz,ALIGNMENT);
    ASSUME_ALIGNED(velx,ALIGNMENT);
    ASSUME_ALIGNED(vely,ALIGNMENT);
    ASSUME_ALIGNED(velz,ALIGNMENT);


    stopwatch.start(TIMER_SEQ);
    for(unsigned int step = 0; step < steps; ++step)
    {
        for (int i = 0; i < num_particles; ++i)
        {
            float fx = 0.0f;
            float fz = 0.0f;
            float fy = 0.0f;

            for (int j = 0; j < num_particles; ++j)
            {
                float rx;
                float ry;
                float rz;

                rx = posx[j] - posx[i];
                ry = posy[j] - posy[i];
                rz = posz[j] - posz[i];

                float dist2 = rx * rx + ry * ry + rz * rz + eps2;
                float dist6 = dist2 * dist2 * dist2;
                float invDistCube = 1.0f/sqrt(dist6);

                fx += mass[j] * invDistCube * rx;
                fy += mass[j] * invDistCube * ry;
                fz += mass[j] * invDistCube * rz;
            }

            velx[i] += fx * g * dtime;
            vely[i] += fy * g * dtime;
            velz[i] += fz * g * dtime;

            posx[i] += velx[i] * dtime;
            posy[i] += vely[i] * dtime;
            posz[i] += velz[i] * dtime;


        }
    }
    stopwatch.stop(TIMER_SEQ);


    // checksum
    float checksum = 0.0f;
    for(auto& p : particles)
    {
        checksum = std::accumulate(p.begin(),p.end(),checksum,
                        [](const float p1, const float p2) -> float
                        {
                            return std::abs(p1) + std::abs(p2);
                        });
    }

    double flops = (num_particles * ( num_particles*22 + 15));
    double flops_per_sec = flops / stopwatch.getTime(TIMER_SEQ);

    cout << 1 << delim
         << 1 << delim
         << num_particles << delim
         << steps << delim
         << checksum   << delim
         << stopwatch.getTime(TIMER_SEQ) << delim
         << flops_per_sec*steps / 1000000000 << " GFLOPS/s" << delim
         << endl;
}

void NBody::runPar(const unsigned int steps, const float dtime)
{
    if( num_particles == 0)
        return;

    stopwatch.start(TIMER_TOTAL);
    stopwatch.start(TIMER_INIT);

    avector<float> fxv(num_particles);
    avector<float> fzv(num_particles);
    avector<float> fyv(num_particles);

    resizeParticleStorage(num_particles);

    float* mass = particles[WEIGHT].data();
    float* posx = particles[POS_X].data();
    float* posy = particles[POS_Y].data();
    float* posz = particles[POS_Z].data();
    float* velx = particles[VEL_X].data();
    float* vely = particles[VEL_Y].data();
    float* velz = particles[VEL_Z].data();

    float* Fx = fxv.data();
    float* Fy = fyv.data();
    float* Fz = fzv.data();

    ASSUME_ALIGNED(mass,ALIGNMENT);
    ASSUME_ALIGNED(posx,ALIGNMENT);
    ASSUME_ALIGNED(posy,ALIGNMENT);
    ASSUME_ALIGNED(posz,ALIGNMENT);
    ASSUME_ALIGNED(velx,ALIGNMENT);
    ASSUME_ALIGNED(vely,ALIGNMENT);
    ASSUME_ALIGNED(velz,ALIGNMENT);
    ASSUME_ALIGNED(Fx,ALIGNMENT);
    ASSUME_ALIGNED(Fy,ALIGNMENT);
    ASSUME_ALIGNED(Fz,ALIGNMENT);

    //const unsigned int num_particles = num_particles;
    const float eps2 = 0.001; // softening factor
    //const float eps2 = 0.005f;
    const float g = GRAVITATIONAL_CONSTANT;

    stopwatch.stop(TIMER_INIT);

    stopwatch.start(TIMER_LOOP);

    #pragma omp parallel
    for(unsigned int step = 0; step < steps; ++step)
    {
        #pragma omp for schedule(static)
        for (int i = 0; i < num_particles; ++i)
        {
            alignas(ALIGNMENT) float fx = 0.0f;
            alignas(ALIGNMENT) float fz = 0.0f;
            alignas(ALIGNMENT) float fy = 0.0f;

            #pragma omp simd reduction(+:fx,fy,fz) aligned(posx,posy,posz:ALIGNMENT)
            for (int j = 0; j < num_particles; ++j)
            {
                alignas(ALIGNMENT) float rx;
                alignas(ALIGNMENT) float ry;
                alignas(ALIGNMENT) float rz;

                // distance
                rx = posx[j] - posx[i];
                ry = posy[j] - posy[i];
                rz = posz[j] - posz[i];

                float dist2 = rx * rx + ry * ry + rz * rz + eps2;
                float dist6 = dist2 * dist2 * dist2;
                float invDistCube = 1.0f/sqrt(dist6);

                fx += mass[j] * invDistCube * rx;
                fy += mass[j] * invDistCube * ry;
                fz += mass[j] * invDistCube * rz;
            }

#if 0   // opt faza 2
            velx[i] += fx * g * dtime;
            vely[i] += fy * g * dtime;
            velz[i] += fz * g * dtime;
#else   // opt faza 3

            Fx[i] = fx;
            Fy[i] = fy;
            Fz[i] = fz;
        }

        #pragma omp for simd schedule(static)
        for (int i = 0; i < num_particles; ++i)
        {
            velx[i] += Fx[i] * g * dtime;
            vely[i] += Fy[i] * g * dtime;
            velz[i] += Fz[i] * g * dtime;
#endif

            posx[i] += velx[i] * dtime;
            posy[i] += vely[i] * dtime;
            posz[i] += velz[i] * dtime;
        }
    }
    stopwatch.stop(TIMER_LOOP);
    stopwatch.stop(TIMER_TOTAL);


    double flops = (num_particles * ( num_particles*22 + 15));
    double flops_per_sec = flops / stopwatch.getTime(TIMER_LOOP);

    int omp_threads = omp_get_max_threads();

    // checksum
    float checksum = 0.0f;
    for(auto& p : particles)
    {
        checksum = std::accumulate(p.begin(),p.end(),checksum,
                        [](const float p1, const float p2) -> float
                        {
                            return std::abs(p1) + std::abs(p2);
                        });
    }

    cout << world_size << delim
         << omp_threads << delim
         << num_particles << delim
         << steps << delim
         << checksum << delim
         << stopwatch.getTime(TIMER_LOOP) << delim
         << flops_per_sec*steps / 1000000000 << " GFLOPS/s" << delim
         << endl;
}

void NBody::runParMPI(const unsigned int steps, const float dtime)
{
    MPI_Bcast(&num_particles,1,MPI_UNSIGNED,0,MPI_COMM_WORLD);

    if( num_particles == 0)
        return;

    const int my_part_start = my_rank * (num_particles / world_size);
    const int my_part_end = my_part_start + (num_particles / world_size);
    const int my_dataset_size = my_part_end - my_part_start;

    if(((my_dataset_size * world_size) != num_particles) )
    {
     //   MPI_Abort(MPI_COMM_WORLD,666);
    }

    stopwatch.start(TIMER_TOTAL);
    stopwatch.start(TIMER_INIT);

    avector<float> fxv(my_dataset_size);
    avector<float> fzv(my_dataset_size);
    avector<float> fyv(my_dataset_size);

    resizeParticleStorage(num_particles);

    for(avector<float>& v : particles)
    {
        MPI_Bcast(v.data(),num_particles,MPI_FLOAT,0,MPI_COMM_WORLD);
    }

    float* mass = particles[WEIGHT].data();
    float* posx = particles[POS_X].data();
    float* posy = particles[POS_Y].data();
    float* posz = particles[POS_Z].data();
    float* velx = particles[VEL_X].data();
    float* vely = particles[VEL_Y].data();
    float* velz = particles[VEL_Z].data();

    float* Fx = fxv.data();
    float* Fy = fyv.data();
    float* Fz = fzv.data();

    ASSUME_ALIGNED(mass,ALIGNMENT);
    ASSUME_ALIGNED(posx,ALIGNMENT);
    ASSUME_ALIGNED(posy,ALIGNMENT);
    ASSUME_ALIGNED(posz,ALIGNMENT);
    ASSUME_ALIGNED(velx,ALIGNMENT);
    ASSUME_ALIGNED(vely,ALIGNMENT);
    ASSUME_ALIGNED(velz,ALIGNMENT);
    ASSUME_ALIGNED(Fx,ALIGNMENT);
    ASSUME_ALIGNED(Fy,ALIGNMENT);
    ASSUME_ALIGNED(Fz,ALIGNMENT);

    //const unsigned int num_particles = num_particles;
    const float eps2 = 0.001; // softening factor
    //const float eps2 = 0.005f;
    const float g = GRAVITATIONAL_CONSTANT;

    stopwatch.stop(TIMER_INIT);

    stopwatch.start(TIMER_LOOP);

    #pragma omp parallel
    for(unsigned int step = 0; step < steps; ++step)
    {
        #pragma omp for schedule(static)
        for (int i = my_part_start; i < my_part_end; ++i)
        {
            alignas(ALIGNMENT) float fx = 0.0f;
            alignas(ALIGNMENT) float fz = 0.0f;
            alignas(ALIGNMENT) float fy = 0.0f;

            #pragma omp simd reduction(+:fx,fy,fz) aligned(posx,posy,posz:ALIGNMENT)
            for (int j = 0; j < num_particles; ++j)
            {
                alignas(ALIGNMENT) float rx;
                alignas(ALIGNMENT) float ry;
                alignas(ALIGNMENT) float rz;

                // distance
                rx = posx[j] - posx[i];
                ry = posy[j] - posy[i];
                rz = posz[j] - posz[i];

                float dist2 = rx * rx + ry * ry + rz * rz + eps2;
                float dist6 = dist2 * dist2 * dist2;
                float invDistCube = 1.0f/sqrt(dist6);

                fx += mass[j] * invDistCube * rx;
                fy += mass[j] * invDistCube * ry;
                fz += mass[j] * invDistCube * rz;
            }

            Fx[i-my_part_start] = fx;
            Fy[i-my_part_start] = fy;
            Fz[i-my_part_start] = fz;
        }

        #pragma omp for simd schedule(static)
        for (int i = my_part_start; i < my_part_end; ++i)
        {
            velx[i] += Fx[i-my_part_start] * g * dtime;
            vely[i] += Fy[i-my_part_start] * g * dtime;
            velz[i] += Fz[i-my_part_start] * g * dtime;

            posx[i] += velx[i] * dtime;
            posy[i] += vely[i] * dtime;
            posz[i] += velz[i] * dtime;
        }

        #pragma omp master
        {
            stopwatch.start(TIMER_MPI);
            for(int i=POS_X; i<=POS_Z; ++i)
            {
                MPI_Allgather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,particles[i].data(),my_dataset_size,MPI_FLOAT,MPI_COMM_WORLD);
            }
            stopwatch.stop(TIMER_MPI);
        }

        #pragma omp barrier
    }
    stopwatch.stop(TIMER_LOOP);
    stopwatch.stop(TIMER_TOTAL);


    MPI_Barrier(MPI_COMM_WORLD);

	int omp_threads = omp_get_max_threads();

    if(my_rank == 0)
    {
        // collect velocity
        for(int i=VEL_X; i<=VEL_Z; ++i)
            MPI_Gather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,particles[i].data(),my_dataset_size,MPI_FLOAT,0,MPI_COMM_WORLD);

        // checksum
        float checksum = 0.0f;
        for(auto& p : particles)
        {
            checksum = std::accumulate(p.begin(),p.end(),checksum,
                            [](const float p1, const float p2) -> float
                            {
                                return std::abs(p1) + std::abs(p2);
                            });
        }

        

        double flops = (num_particles * ( num_particles*22 + 15));
        double flops_per_sec = flops / stopwatch.getTime(TIMER_LOOP);

        cout << world_size << delim
             << omp_threads << delim
             << num_particles << delim
             << steps << delim
             << checksum << delim
             << stopwatch.getTime(TIMER_MPI) << delim
             << stopwatch.getTime(TIMER_LOOP) << delim
             << flops_per_sec*steps / 1000000000 << " GFLOPS/s" << delim
             << endl;
    }
    else
    {
        for(int i=VEL_X; i<=VEL_Z; ++i)
            MPI_Gather(particles[i].data()+my_part_start,my_dataset_size,MPI_FLOAT,particles[i].data(),0,MPI_FLOAT,0,MPI_COMM_WORLD);
    }
}

void NBody::printParticles() const
{
    for(const avector<float>& av : particles)
    {
        copy(av.begin(),av.end(),ostream_iterator<float>(cout,"\t"));
        cout << endl;
    }
}

float NBody::calcMaxDiff(NBody &ref, float epsilon)
{
    if(num_particles != ref.getNumParticles())
        return -1.0f;

    float diff = 0.0f;

    for (int a = PARTICLE_ATTRIB_FIRST; a < PARTICLE_ATTRIBS_MAX; ++a)
    {
        if( a == WEIGHT)
            continue;

        avector<float>& r = ref.getParticleData(static_cast<PARTICLE_ATTRIB>(a));
        avector<float>& o = getParticleData(static_cast<PARTICLE_ATTRIB>(a));

        for(int p=0; p < num_particles; ++p)
        {
            diff = max(abs(o[p] - r[p]),diff);
        }
    }

    return diff;
}

float NBody::calcTotalErrorSum(NBody &ref)
{
    float diff = 0.0f;

    for (int a = PARTICLE_ATTRIB_FIRST; a < PARTICLE_ATTRIBS_MAX; ++a)
    {
        avector<float>& r = ref.getParticleData(static_cast<PARTICLE_ATTRIB>(a));
        avector<float>& o = getParticleData(static_cast<PARTICLE_ATTRIB>(a));

        for(int p=0; p < num_particles; ++p)
        {
            diff += fabs(o[p] - r[p]);
        }
    }

    return diff;
}

void NBody::printTimes()
{
    cout << "===Times===\n";
    cout << "Seq " << stopwatch.getTime(TIME_SEQ) << "\n";
    cout << "Par loop " << stopwatch.getTime(TIME_PAR_LOOP) << endl;
}

void NBody::loadParticles(const char *filename)
{
    ifstream ifs(filename);

    ifs.exceptions();

    if (!ifs.is_open())
    {
        throw runtime_error(std::string("Can't open file ") + filename);
    }

    num_particles = 0;
    ifs >> num_particles;

    for(avector<float>& av : particles)
    {
        av.clear();
        av.reserve(num_particles);
        copy_n(istream_iterator<float>(ifs),num_particles,back_inserter(av));
    }

    ifs.close();
}

void NBody::loadParticlesBinary(const char *filename)
{
    ifstream ifs(filename,ios::binary);

    if (!ifs.is_open())
    {
        throw runtime_error(std::string("Can't open file ") + filename);
    }

    num_particles = 0;
    int num_attributes = 0;
    ifs.read(reinterpret_cast<char*>(&num_attributes),sizeof(int));
    ifs.read(reinterpret_cast<char*>(&num_particles),sizeof(unsigned int));

    for(avector<float>& av : particles)
    {
        av.clear();
        av.reserve(num_particles);
        for(int i=0; i<num_particles; ++i)
        {
            float v;
            ifs.read(reinterpret_cast<char*>(&v),sizeof(float));
            av.push_back(v);
        }
        //copy_n(istream_iterator<float>(ifs),num_particles,back_inserter(av));
    }

    ifs.close();
}

void NBody::saveParticles(const char *filename)
{
    ofstream ofs(filename);

    if(!ofs.is_open())
    {
        throw runtime_error(std::string("Can't open file ") + filename);
    }

    ofs << num_particles << endl;

    for(avector<float>& av : particles)
    {
        copy(av.begin(),av.end(),ostream_iterator<float>(ofs," "));
        ofs << '\n';
    }

    ofs.close();
}

void NBody::saveParticlesBinary(const char *filename)
{
    ofstream ofs(filename,fstream::binary);

    if(!ofs.is_open())
    {
        throw runtime_error(std::string("Can't open file ") + filename);
    }

    int32_t attributes = PARTICLE_ATTRIBS_MAX;
    ofs.write(reinterpret_cast<char*>(&attributes),sizeof(int32_t));
    ofs.write(reinterpret_cast<char*>(&num_particles),sizeof(uint32_t));

    for(avector<float>& av : particles)
    {
        for(float& f : av)
        {
            ofs.write(reinterpret_cast<char*>(&f),sizeof(float));
        }
    }

    ofs.close();
}

void NBody::generateRandom(unsigned int n, float weight_min, float weight_max, float world_size)
{
#ifndef __MIC__
    num_particles = n;

    default_random_engine gen;
    //uniform_real_distribution<float> dist(-world_size,world_size);
    normal_distribution<float> dist(-world_size,world_size);

    auto generator = bind(dist,ref(gen));

    //for(avector<float>& av : particles)
    for(int i = PARTICLE_ATTRIB_FIRST; i < PARTICLE_ATTRIBS_MAX; ++i)
    {
        avector<float>& av = particles[i];
        av.clear();
        av.reserve(n);
        if(i==WEIGHT)
        {
            fill_n(back_inserter(av),n,10.0f);
        }
        else
            generate_n(back_inserter(av),n,generator);
    }
#endif
}
