#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <vector>

#define SCOPED_TIMER_START(s,n)   StopwatchScopedTimer((&s),n)

class Stopwatch
{
private:
    std::vector<double> _start;
    std::vector<double> _end;
    std::vector<double> _total;

public:
    Stopwatch(int num_timers);

    void start(int name = 0);
    void stop(int name = 0);

    double getTime(int name = 0);

};

class StopwatchScopedTimer
{
    int n;
    Stopwatch* s;

public:
    StopwatchScopedTimer(Stopwatch* stopwatch, int name) : s(stopwatch), n(name)
    {
        s->start(name);
    }

    ~StopwatchScopedTimer()
    {
        s->stop(n);
    }
};

#endif // STOPWATCH_H
