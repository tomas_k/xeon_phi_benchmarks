#ifndef ALIGNEDALLOCATOR
#define ALIGNEDALLOCATOR

#ifndef ALIGNMENT
    #ifdef __MIC__
        #define DEFAULT_ALIGNMENT   64
    #else
        #define DEFAULT_ALIGNMENT   32
    #endif
#endif

#ifndef ALIGNMENT
#define ALIGNMENT DEFAULT_ALIGNMENT
#endif

#ifdef __INTEL_COMPILER
#define ASSUME_ALIGNED(ptr,alignment)	__assume_aligned((ptr),(alignment))
#elif __GNUC__
#define ASSUME_ALIGNED(ptr,alignment)	ptr = static_cast<typeof((ptr))>(__builtin_assume_aligned((ptr),(alignment)))
#endif


#include <cstddef>
#include <new>
#include <type_traits>

#include <vector>

// TODO: asserty
// TODO: in-place construction
// TODO: void specializacia

namespace detail
{
    void* allocate_aligned(size_t size, size_t alignment);
    void deallocate_aligned(void* p);
}

template<typename T, int Alignment = ALIGNMENT/*, typename = std::enable_if<std::is_fundamental<T>::value>*/>
struct AlignedAllocator
{
public:
    typedef T               value_type;
    typedef T*              pointer;
    typedef const T*        const_pointer;
    typedef T&              reference;
    typedef const T&        const_reference;
    typedef std::size_t     size_type;
    typedef std::ptrdiff_t  difference_type;

    template <class U> struct rebind { typedef AlignedAllocator<U> other; };

public:
    AlignedAllocator() {}

    template<class U>
    AlignedAllocator(const AlignedAllocator<U>& other) {}

    pointer allocate(size_type n) throw(std::bad_alloc)
    {
        void* ptr = detail::allocate_aligned(n*sizeof(value_type),Alignment);
        if(ptr == nullptr)
            throw std::bad_alloc();

        pointer p = reinterpret_cast<pointer>(ptr);

        // NUMA first touch strategy
        #pragma omp parallel for
        for(int i=0; i<n; ++i)
            p[i] = static_cast<value_type>(0);

        return p;
    }

    void deallocate(pointer p, size_type n)
    {
        detail::deallocate_aligned(p);
    }

    void destroy( pointer p )
    {
        // fundamental type doesnt have constructor
    }

    void construct( pointer p, const_reference val = 0 )
    {
        *p = val;
    }

    size_type max_size() const
    {
        // FIXME
        std::vector<value_type> a;
        return a.max_size();
    }
};

template <class T, int TAlignment, class U, int UAlignment>
bool operator ==(const AlignedAllocator<T,TAlignment>&, const AlignedAllocator<U,UAlignment>&)
{
    return (TAlignment == UAlignment) && std::is_same<T,U>::value;
}

template <class T, int TAlignment, class U, int UAlignment>
bool operator !=(const AlignedAllocator<T,TAlignment>&, const AlignedAllocator<U,UAlignment>&)
{
    return (TAlignment != UAlignment) || !std::is_same<T,U>::value;
}


#endif // ALIGNEDALLOCATOR

