#!/bin/sh
#PBS -A OPEN-6-11
#PBS -l select=1:ncpus=24:mpiprocs=2:ompthreads=12

PROG=$1
ITERATIONS=$2
LAST=$3

export OMP_NUM_THREADS=24
export KMP_AFFINITY=granularity=fine,compact

for i in 1024 2048 4096 8192 18000 24000
do
    ./$PROG $ITERATIONS ../p$i.txt out-$PROG-$i.txt | tee -a results-$PROG.txt

    if [ $i -eq $LAST ]; then break; fi

done
