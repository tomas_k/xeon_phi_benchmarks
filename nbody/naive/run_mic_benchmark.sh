#!/bin/sh

cd ~/xeon_phi_benchmarks/nbody/naive-openmp-mpi
. ./setup_mic_env.sh

export KMP_AFFINITY=granularity=core,scatter

for i in 60 120 180 240
do
export OMP_NUM_THREADS=$i
for j in 1024 2048 4096 8192
do
./nbody_mic 5000 ../p$j-128.txt x | tee -a results_mic_mp2_scat.txt
done
./nbody_mic 5000 ../p18000-128.txt x | tee -a results_mic_mp2_scat.txt
./nbody_mic 5000 ../p24000-512.txt x | tee -a results_mic_mp2_scat.txt
done
