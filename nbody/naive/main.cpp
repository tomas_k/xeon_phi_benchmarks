#include <iostream>

#include <mpi.h>

#include "nbodysimulation.h"

using namespace std;

int main(int argc, char** argv)
{
    int provided;
    //MPI_Init(&argc,&argv);
    MPI_Init_thread(&argc,&argv,MPI_THREAD_FUNNELED,&provided);

    int rank = 0;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

//    if(provided != MPI_THREAD_SERIALIZED){
//        if(rank==0) cout << "MPI_Init_thread requested not provided (" << provided << ")" << endl;
//    }
//    else{
//        if(rank==0)  cout << "MPI_Init_thread requested provided (" << provided << ")" << endl;
//    }

    if(argc != 4)
    {
        cout << "Usage:\n" << argv[0]
             << " <steps> <input filename> <output filename>"
             << endl;

        return 0;
    }

    int steps = atoi(argv[1]);
    const char* input_file = argv[2];
    const char* output_file = argv[3];

    NBody nbody;

    const float dt = 0.005;

    if(rank == 0)
    {
        nbody.loadParticles(input_file);
    }

    nbody.runParMPI(steps,dt);

    if(rank == 0)
    {
        nbody.saveParticles(output_file);
    }

    MPI_Finalize();
    return 0;
}

