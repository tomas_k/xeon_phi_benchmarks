#!/bin/sh
#PBS -A OPEN-6-11
#PBS -l select=1:ncpus=24
#PBS -l walltime=30:00

PROG=$1
ITERATIONS=$2
LAST=$3

export KMP_AFFINITY=granularity=fine,scatter

for t in 1 2 4 8 16 24
do
export OMP_NUM_THREADS=$t

for i in 1024 2048 4096 8192 18000
do
    ./$PROG $ITERATIONS ../p$i.txt out-$PROG-$i.txt | tee -a results-$PROG-scaling.txt

    if [ $i -eq $LAST ]; then break; fi

done

done
