
#include "alignedallocator.h"

#include <cstdlib>
#include <cassert>

void* detail::allocate_aligned(size_t size, size_t alignment)
{
    if(size == 0)
        return nullptr;

    void* ptr;
    int ret = posix_memalign(&ptr,alignment,size);

    if(ret != 0)
        return nullptr;

 //   assert(((uintptr_t)ptr % alignment) == 0);

    return ptr;
}

void detail::deallocate_aligned(void* p)
{
    free(p);
}

