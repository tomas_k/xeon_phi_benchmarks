#include "stopwatch.h"

#include <limits>

#include <omp.h>


using namespace std;

Stopwatch::Stopwatch(int num_timers)
{
    _start.resize(num_timers);
    _end.resize(num_timers);
    _total.resize(num_timers);
}

void Stopwatch::start(int name)
{
    _start[name] = omp_get_wtime();
}

void Stopwatch::stop(int name)
{
    double end = omp_get_wtime();

    _total[name] += end - _start[name];

    _start[name] = 0;

}

double Stopwatch::getTime(int name)
{
    return _total[name];
}
