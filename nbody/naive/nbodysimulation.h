#ifndef NBODYSIMULATION_H
#define NBODYSIMULATION_H

#include "alignedallocator.h"

#include "stopwatch.h"

#include <vector>

template<typename T>
using avector = std::vector<T, AlignedAllocator<T, DEFAULT_ALIGNMENT>>;

class NBody
{
public:
    enum PARTICLE_ATTRIB{
        PARTICLE_ATTRIB_FIRST = 0,
        WEIGHT = PARTICLE_ATTRIB_FIRST,
        POS_X,
        POS_Y,
        POS_Z,
        VEL_X,
        VEL_Y,
        VEL_Z,

        PARTICLE_ATTRIBS_MAX,
    };

private:
    unsigned int    num_particles;

    // "structure" of arrays
    avector<float> particles[PARTICLE_ATTRIBS_MAX];

    int my_rank;
    int world_size;

    Stopwatch stopwatch;

    enum TIMERS_NAMES
    {
        TIMER_TOTAL = 0,
        TIMER_INIT,
        TIMER_LOOP,
        TIMER_MPI,
        TIMER_WAIT,
        TIMER_SEQ,

        TIMERS_NAMES_MAX
    };

    static constexpr int TIME_SEQ = 1;
    static constexpr int TIME_PAR_INIT = 2;
    static constexpr int TIME_PAR_LOOP = 3;
    static constexpr int TIME_PAR_COMM = 4;

private:
    float*  getDataPtr(PARTICLE_ATTRIB which)  { return particles[which].data(); }

    void resizeParticleStorage(size_t size);

public:
    NBody();

    unsigned int getNumParticles()  const { return num_particles; }
    avector<float>& getParticleData(PARTICLE_ATTRIB attrib) { return particles[attrib]; }

    void loadParticles(const char* filename);
    void loadParticlesBinary(const char* filename);
    void saveParticles(const char* filename);
    void saveParticlesBinary(const char* filename);
    void generateRandom(unsigned int n, float weight_min, float weight_max, float world_size);

    void runSeq(const unsigned int steps, const float dtime);
    void runPar(const unsigned int steps, const float dtime);
    void runParMPI(const unsigned int steps, const float dtime);
    void runParMPIOverlapped(const unsigned int steps, const float dtime);

    void printParticles() const;

    float calcMaxDiff(NBody& ref, float epsilon = 0.005f);
    float calcTotalErrorSum(NBody& ref);

    void printTimes();
};

#endif // NBODYSIMULATION_H
