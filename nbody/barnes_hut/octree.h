#ifndef OCTREE_H
#define OCTREE_H

#include <mpi.h>

#include "alignedallocator.h"

#include <vector>
#include <cstdint>

#include "nbodysimulation.h"

template<typename T> using AlignedVector = std::vector<T, AlignedAllocator<T, DEFAULT_ALIGNMENT>>;

class Octree
{
private:
    enum PARTICLE_ATTRIB
    {
        PARTICLE_ATTRIB_FIRST = 0,

        MASS = PARTICLE_ATTRIB_FIRST,                   // 0
        POS_X,                                          // 1
        POS_Y,                                          // 2
        POS_Z,                                          // 3

        PARTICLE_ATTRIB_OCTREE_NODE_ATTRIBS_MAX,        // 4

        VEL_X = PARTICLE_ATTRIB_OCTREE_NODE_ATTRIBS_MAX,// 4
        VEL_Y,                                          // 5
        VEL_Z,                                          // 6

        PARTICLE_ATTRIBS_MAX,                           // 7
    };

    /********************************************************
     *  Octree nodes SoA
     * *****************************************************/
    // each octree node contains MASS, POS_X, POS_Y, POS_Z and COUNT
    // nodes[LEVELS][PARTICLE_ATTRIBS][node]
    std::vector<std::vector<AlignedVector<float>>> nodes;
    // nodes_particle_counts[LEVEL][node]
    std::vector<AlignedVector<uint32_t>> nodes_particle_counts;
    // last level - 1 contains indices particle_storage
    AlignedVector<uint32_t> nodes_groups_indices;

    // inversed squared diameter of each octree node
    // same level = same diameter
    AlignedVector<float> octree_nodes_diameters_inv2;

    /********************************************************
     *  Particles SoA
     * *****************************************************/
    //using particles_soa_t = AlignedVector<float>[PARTICLE_ATTRIBS_MAX];
    typedef AlignedVector<float> particles_soa_t[PARTICLE_ATTRIBS_MAX];

    //particles_soa_t particles_storage[2];   // particles_storage[PARTICLE_ATTRIBS][particles]

    particles_soa_t particles_storage_local;
    particles_soa_t particles_storage_global;

    AlignedVector<uint32_t> sfc_keys_local;
    AlignedVector<uint32_t> sfc_keys_global;
    AlignedVector<uint32_t> sort_helper_indices;    // helper, used for sorting soa particles by sfc keys

    std::vector<AlignedVector<uint32_t>> histogram_private;

    AlignedVector<float> local_forces[3];

    std::vector<AlignedVector<char>> traverse_list;

private:
    int my_rank;
    int world_size;

    uint32_t    num_local_particles;
    uint32_t    num_global_particles;

    uint8_t     num_levels;

    uint32_t    octree_dim;   // number of nodes on edge @ lower level
    uint32_t    bits_per_dim;

    uint32_t    histogram_size; // sfc keys histogram


    AlignedVector<uint32_t>  mergeSplitRecvBuffer;  // recvbuf for merge split sort
    AlignedVector<uint32_t>  mergeSplitBuffer;      // buf for merging

    float softening2;   // softening^2
    float inv_theta2;   // 1 / theta^2

    // statistics
    AlignedVector<uint64_t> traversal_stats;

private:
    int comm_node;
    MPI_Request request_gathersfc;
    MPI_Request request_gatherparticles[7];
    MPI_Request request_gatherposvel[6];

public:
    void mpiwaitgatherposvel()
    {
        MPI_Waitall(6,request_gatherposvel,MPI_STATUSES_IGNORE);
    }

private:

    /**
     * @brief traverse
     * @param fx
     * @param fy
     * @param fz
     * @param level
     * @param node
     * @param particle
     */
    void traverse(float& fx, float& fy, float& fz, uint8_t level, uint32_t node,uint32_t particle);
    /**
     * @brief getParent
     * @param index Index of octree node particle
     * @return Index of parent octree node on previous level
     */
    uint32_t getParent(uint32_t index) const { return index/8; }

    /**
     * @brief getFirstChild
     * @param index
     * @return Index of First child
     */
    uint32_t getFirstChild(uint32_t index)  const { return 8*index; }

public:
    Octree(uint8_t numLevels, int myRank, int worldSize, int async_comm);
    ~Octree();

    void setParticlesCount(uint32_t numGlobalParticles, uint32_t numLocalParticles) { num_global_particles = numGlobalParticles; num_local_particles = numLocalParticles; }
    void allocateBuffers();

    void computeLocalAABB(float local_min[3], float local_max[3]);

    // compute global axis aligned boundong cube - top level octree node
    void computeGlobalAABB(float local_min[3], float local_max[3], float center[3], float& diameter);

    void computeLocalSFCKeys(float center[3], float diameter);

    // sort sorting helper indices by sfc keys, sorping helper indices are used to rearrange soa particles
    void buildLocalSortedSFCIndices();

    void rearrangeLocalParticles();

    void buildLocalOctree();

    // recursive traversal - depth first
    void computeForces();

    // iteratice traversal - breadth first
    void computeForces2();

    // update position and velocity of local particles
    void updateLocalVelPos(float dtime);

    void mpiBroadcastParticles();   // bcast global particles
    void mpiScatterParticles();     // scatter global particles to local
    void mpiAllGatherSFCKeys();
    void mpiAllGatherParticles();   // gather local to global (pos,vel,mas)
    void mpiAllGatherPosVel();      // gather local to global (pos,vel)

    void mpiSortSFCIndices();       // parallel mpi sort - merge split sort
    void mpiBuildGlobalOctree();    // reduce local octrees to global octree

    uint32_t generateRandomParticles(uint32_t n, float weight_min, float weight_max, float world_size);
    uint32_t loadParticles(const char* filename);
    void saveParticles(const char* filename);

    // update precomputed octree, part of one of premature optimizations
    void updateOctreeNodeDimensions(float diameter);

    void printLocalParticles();
    void printGlobalParticles();

    float calculateChecksum();

    void setSofteningFactor(float sf)   { softening2 = sf*sf; }
    void setTheta(float th)             { inv_theta2 = 1/(th*th); }

    AlignedVector<uint64_t>& getTraversalStats();

    void dumpState();

private:
    // comparator for sorting SOA particles by key
    template<typename T>
    class NBodySFCComparator
    {
        const AlignedVector<T>& _sfc_keys;
    public:
        NBodySFCComparator(AlignedVector<T>& sfc_keys) : _sfc_keys(sfc_keys) {}

        inline bool operator()(const T& lhs, const T& rhs) const
        {
            return _sfc_keys[lhs] < _sfc_keys[rhs];
        }

    };
};

#endif // OCTREE_H
