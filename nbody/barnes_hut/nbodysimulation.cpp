#include "nbodysimulation.h"

#include "octree.h"
#include "stopwatch.h"

#include "hilbert.h"
//#include "cilkplus/parallel_stable_sort.h"
#include "openmp/parallel_stable_sort.h"

#include <fstream>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <limits>
#include <random>
#include <functional>
#include <iomanip>
#include <numeric>
#include <cmath>
#include <cassert>
#include <iostream>

#include <omp.h>

#define G 1.0f

using namespace std;

#include <mpi.h>

const char* delim = ";";

NBody::NBody(int octree_max_depth, int trav_mode, int comm_mode) : stopwatch(TIMERS_NAMES_MAX)
{
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&world_size);

    octree = new Octree(octree_max_depth,my_rank,world_size,comm_mode);


    num_particles = 0;

    this->trav_mode = trav_mode;
    //    std::ios_base::sync_with_stdio(false);
}

NBody::~NBody()
{
    delete octree;
}

void NBody::runSeq(const unsigned int steps, const float dtime)
{

}

void NBody::runPar(const unsigned int steps, const float dtime, float theta, float softening)
{
//    const int my_part_start = my_rank * (num_particles / world_size);
//    const int my_part_end = my_part_start + (num_particles / world_size);
//    const int my_dataset_size = my_part_end - my_part_start;

    if(my_rank == 0 && num_particles == 0)
    {
        MPI_Abort(MPI_COMM_WORLD,-1);
    }


    /************************************************************
     *
     * **********************************************************/
    uint32_t particles_per_process;

    if(my_rank == 0)
    {
        particles_per_process = num_particles/world_size;
        //assert( (particles_per_process*world_size)==num_particles );
    }

    stopwatch.start(TIMER_TOTAL);
    stopwatch.start(TIMER_INIT);

    MPI_Bcast(&num_particles,1,MPI_UNSIGNED,0,MPI_COMM_WORLD);
    MPI_Bcast(&particles_per_process,1,MPI_UNSIGNED,0,MPI_COMM_WORLD);


    octree->setParticlesCount(num_particles, particles_per_process);
    octree->allocateBuffers();
    octree->mpiBroadcastParticles();

    octree->setSofteningFactor(softening);
    octree->setTheta(theta);

    octree->mpiScatterParticles();

    stopwatch.stop(TIMER_INIT);


    stopwatch.start(TIMER_LOOP);


        for(uint64_t iteration = 0 ; iteration < steps; ++iteration)
        {
            float center[3];
            float diameter;
            float local_min[3];
            float local_max[3];

            stopwatch.start(TIMER_OCTREE_BUILD);

                octree->computeLocalAABB(local_min,local_max);
                octree->computeGlobalAABB(local_min,local_max,center,diameter);

                octree->updateOctreeNodeDimensions(diameter);

                octree->computeLocalSFCKeys(center,diameter);

                octree->mpiAllGatherSFCKeys();

                octree->buildLocalSortedSFCIndices();

                octree->mpiSortSFCIndices();

                octree->rearrangeLocalParticles();

                octree->mpiAllGatherParticles();

                octree->buildLocalOctree();

                octree->mpiBuildGlobalOctree();

            stopwatch.stop(TIMER_OCTREE_BUILD);



            stopwatch.start(TIMER_FORCE_COMPUTATION);
                if(trav_mode)
                    octree->computeForces2();
                else
                    octree->computeForces();
            stopwatch.stop(TIMER_FORCE_COMPUTATION);

            stopwatch.start(TIMER_PARTICLE_UPDATE);
                octree->updateLocalVelPos(dtime);
            stopwatch.stop(TIMER_PARTICLE_UPDATE);


            octree->mpiAllGatherPosVel();
        }

    stopwatch.stop(TIMER_LOOP);

    MPI_Barrier(MPI_COMM_WORLD);

    stopwatch.stop(TIMER_TOTAL);

    octree->mpiwaitgatherposvel();

    uint64_t bh_fc_total = std::accumulate(octree->getTraversalStats().begin(),
                                           octree->getTraversalStats().end(),
                                           0);
    uint64_t bh_fc_iter = bh_fc_total / steps;
    uint64_t naive_fc_iter = num_particles*num_particles;
    uint64_t naive_fc_total = naive_fc_iter*steps;
    float saved_computations = ((naive_fc_iter - bh_fc_iter)*100)/float(naive_fc_iter);

    if(my_rank == 0)
    {
        int omp_threads = omp_get_max_threads();
        float checksum = octree->calculateChecksum();

        cout << "proces" << delim
             << "threads" << delim
             << "num_p" << delim
             << "soft" << delim
             << "theta" << delim
             << "steps" << delim
             << "saved" << delim
             << "check" << delim
             << "TTOTAL" << delim
             << "TLOOP" << delim
             << endl;

        cout << world_size << delim
             << omp_threads << delim
             << num_particles << delim
             << trav_mode << delim
             << softening << delim
             << theta << delim
             << steps << delim
             << saved_computations << delim
             << checksum << delim
             << stopwatch.getTime(TIMER_OCTREE_BUILD) << delim
             << stopwatch.getTime(TIMER_FORCE_COMPUTATION) << delim
             << stopwatch.getTime(TIMER_PARTICLE_UPDATE) << delim
             << stopwatch.getTime(TIMER_MPI) << delim
             << stopwatch.getTime(TIMER_LOOP) << delim
             << endl;
    }
}

void NBody::loadParticles(const char *filename)
{
    num_particles = octree->loadParticles(filename);
}

void NBody::saveParticles(const char *filename)
{
    octree->saveParticles(filename);
}

void NBody::generateRandomParticles(unsigned int n, float weight_min, float weight_max, float world_size)
{
    num_particles = octree->generateRandomParticles(n,weight_min,weight_max,world_size);
}
