#ifndef ALIGNEDVECTOR_HPP
#define ALIGNEDVECTOR_HPP

#include "alignedallocator.h"
//#include "numaallocator.h"

#include <vector>

template<typename T>
using AlignedVector = std::vector<T, AlignedAllocator<T, DEFAULT_ALIGNMENT>>;


#endif // ALIGNEDVECTOR_HPP
