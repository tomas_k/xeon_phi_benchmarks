#include "octree.h"

#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    Octree octree(1,0,1,1);

    if(argc != 4)
    {
        cout << "Usage: " << argv[0] << " <num particles> <output file> <world size>" << endl;
        return -1;
    }

    int particles = atoi(argv[1]);
    float world_size = atof(argv[3]);

    octree.generateRandomParticles(particles,1.0f,10.0f,world_size);

    octree.saveParticles(argv[2]);

    return 0;
}
