#ifndef NBODYSIMULATION_H
#define NBODYSIMULATION_H

#include "stopwatch.h"
#include "util.h"

class Octree;

class NBody
{
private:
    int my_rank;
    int world_size;

    //std::unique_ptr<Octree> octree;
    Octree* octree;
    Stopwatch stopwatch;

    // depth first traversal or breadth first
    int trav_mode;

    // statistics
    uint32_t num_particles;
    int omp_threads;
    float softening_factor;
    float theta;
    float checksum;
    float n_steps;


    enum TIMERS_NAMES
    {
        TIMER_TOTAL = 0,
        TIMER_INIT,
        TIMER_LOOP,
        TIMER_MPI,
        TIMER_WAIT,

        TIMER_OCTREE_BUILD,
        TIMER_FORCE_COMPUTATION,
        TIMER_PARTICLE_UPDATE,

        TIMERS_NAMES_MAX
    };


public:
    NBody(int octree_max_depth, int trav_mode, int comm_mode);
    ~NBody();

    void loadParticles(const char* filename);
    void saveParticles(const char* filename);

    void generateRandomParticles(unsigned int n, float weight_min, float weight_max, float world_size);

    void runSeq(const unsigned int steps, const float dtime);
    void runPar(const unsigned int steps, const float dtime, float theta, float softening);
};

#endif // NBODYSIMULATION_H
