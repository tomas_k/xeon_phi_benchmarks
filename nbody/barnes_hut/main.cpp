#include <iostream>

#include <mpi.h>

#include "nbodysimulation.h"

#define MPI_MASTER    if(rank==0)

using namespace std;

#include <unistd.h>

int main(int argc, char** argv)
{
    int provided;
    //MPI_Init(&argc,&argv);
    MPI_Init_thread(&argc,&argv,MPI_THREAD_FUNNELED,&provided);


    if(argc != 8)
    {
        cout << "Usage:\n" << argv[0]
             << " <steps> <theta> <octree max depth> <input filename> <output filename> <traversal mode (0-depth firts, 1-breadth first)> <communication mode (0-sync, 1-async)>"
             << endl;

        return 0;
    }

    int steps = atoi(argv[1]);
    float theta = atof(argv[2]);
    int octree_depth = atoi(argv[3]);
    const char* input_file = argv[4];
    const char* output_file = argv[5];
    int trav_mode = atoi(argv[6]);
    int comm_mode = atoi(argv[7]);


    int rank = 0;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if(provided != MPI_THREAD_FUNNELED)
    {
        MPI_MASTER
        cout << "MPI_Init_thread requested not provided (" << provided << ")" << endl;
    }

#if 0
    if(rank == 0)
    {
        pid_t pid = getpid();
        cout << "rank " << rank << " PID " << pid << endl;
        volatile bool paused = true;
        while(paused)
        {
            sleep(2);
        }
    }
    else
        cout << rank << " waiting " << endl;

    MPI_Barrier(MPI_COMM_WORLD);
    cout << rank << " continue " << endl;
#endif

    NBody nbody(octree_depth,trav_mode,comm_mode);

    MPI_MASTER
    nbody.loadParticles(input_file);


    nbody.runPar(steps,0.005,theta,0.0001);


    MPI_MASTER
    nbody.saveParticles(output_file);


    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Finalize();
    return 0;
}

