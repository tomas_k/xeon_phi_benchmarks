#include "util.h"

#include "alignedallocator.h"

void reduceMinMax(float* data, int size, float& minimum, float& maximum)
{
    static float _min;
    static float _max;

    #pragma omp simd reduction(min:_min) reduction(max:_max) aligned(data:DEFAULT_ALIGNMENT)
    for(int i=0; i<size; ++i)
    {
        if(data[i] < _min)
            _min = data[i];
        if(data[i] > _max)
            _max = data[i];
    }

    //#pragma omp single
    {
        minimum = _min;
        maximum = _max;
    }
}
