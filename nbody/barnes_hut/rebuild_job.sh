#!/bin/bash
#PBS -A OPEN-6-11
#PBS -N BH_1
#PBS -q qfree
#PBS -l walltime=00:01:00
#PBS -l select=1:ncpus=24:accelerator=True:naccelerators=2

module load intel

cd "$PBS_O_WORKDIR"

make clean
make
make mic

rm *.o *.mico
