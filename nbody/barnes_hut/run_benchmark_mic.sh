#!/bin/sh
#PBS -A OPEN-6-11
#PBS -l select=1:ncpus=24
#PBS -l walltime=30:00

PROG=$1
ITERATIONS=$2
MODE=$3
DEPTH=$4
LAST=$5
REPEATS=$6

for theta in "0.5"
do
#export OMP_NUM_THREADS=24

for i in 1024 2048 4096 8192 18000
do
    for t in 60 120 180 240
    do
        export OMP_NUM_THREADS=$t

        for r in $(seq $REPEATS)
        do
            mpirun ./$PROG $ITERATIONS $theta $DEPTH ../p$i.txt out-$PROG-$i.txt $MODE | tee -a results-$PROG-$DEPTH.txt
        done
    done


    if [ $i -eq $LAST ]; then break; fi

done

done
