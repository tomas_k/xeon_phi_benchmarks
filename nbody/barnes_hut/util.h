#ifndef UTIL_H
#define UTIL_H

#include <iterator>
#include "alignedvector.hpp"

void reduceMinMax(float* data, int size, float& minimum, float& maximum);

#endif // UTIL_H
