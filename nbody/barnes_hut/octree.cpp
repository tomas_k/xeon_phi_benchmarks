#include "octree.h"

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <limits>
#include <functional>

#ifndef __MIC__
#include <random>
#endif

#include <cstdio>
#include <cmath>
#include <cassert>

#include <mpi.h>
#include <omp.h>

#include "util.h"

#include "hilbert.h"
#include "openmp/parallel_stable_sort.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::runtime_error;

//#define COLLECT_TRAVERSE_STATS

//#define USE_MORTON_SFC


#define G 1.0f

// transform 1101b to  001001000001b
//   1  1  0  1 b
// 001001000001 b
#pragma omp declare simd notinbranch
uint32_t dilate3(const uint32_t value)
{
    uint32_t x;
    x = value & 0x03FF; // uses 10 bits only
    x = ((x << 16) + x) & 0xFF0000FF;
    x = ((x << 8) + x) & 0x0F00F00F;
    x = ((x << 4) + x) & 0xC30C30C3;
    x = ((x << 2) + x) & 0x49249249;
    return x;
}

#pragma omp declare simd notinbranch
uint32_t get_key_morton(uint32_t x, uint32_t y, uint32_t z) {
  uint32_t key, key1;

  key  = dilate3(x);

  key1 = dilate3(y);
  key = key | (key1 << 1);

  key1 = dilate3(z);
  key = key | (key1 << 2);

  return key;
}


Octree::Octree(uint8_t numLevels, int myRank, int worldSize, int async_comm)
{
    num_levels = numLevels;

    nodes.resize(num_levels);
    nodes_particle_counts.resize(numLevels);
    traverse_list.resize(num_levels);

    for(int level=0; level<numLevels; ++level)
    {
        nodes[level].resize(PARTICLE_ATTRIB_OCTREE_NODE_ATTRIBS_MAX);

        for(int attrib = PARTICLE_ATTRIB_FIRST; attrib<PARTICLE_ATTRIB_OCTREE_NODE_ATTRIBS_MAX; ++attrib)
        {
            nodes[level][attrib].resize(std::pow(8,level));
        }
        nodes_particle_counts[level].resize(std::pow(8,level));
        traverse_list[level].resize(std::pow(8,level),-1);
    }

    nodes_groups_indices.resize(std::pow(8,numLevels-1));


    octree_dim = std::pow(2,num_levels-1);
    bits_per_dim = std::log2(octree_dim);

    my_rank = myRank;
    world_size = worldSize;

    traversal_stats.resize(num_levels);

    comm_node = async_comm;

    request_gathersfc = MPI_REQUEST_NULL;
    std::fill_n(request_gatherparticles,7,MPI_REQUEST_NULL);
    std::fill_n(request_gatherposvel,6,MPI_REQUEST_NULL);
}

Octree::~Octree()
{

}

void Octree::allocateBuffers()
{
    if(my_rank != 0) // mpi master should load particles from file
    {
        for(auto& av : particles_storage_global)
        {
            av.resize(num_global_particles);
        }
    }

    for(auto& av : particles_storage_local)
    {
        av.resize(num_local_particles);
    }

    sfc_keys_global.resize(num_global_particles,-1);
    sfc_keys_local.resize(num_local_particles,-1);

    sort_helper_indices.resize(num_local_particles);

    mergeSplitRecvBuffer.resize(num_local_particles);
    mergeSplitBuffer.resize(2*num_local_particles);

    int omp_threads = omp_get_max_threads();
    histogram_size = nodes_groups_indices.size();

    histogram_private.resize(omp_threads);
    for(auto& av : histogram_private)
    {
        av.resize(histogram_size);
    }

    for(int i=0; i<3; ++i)
        local_forces[i].resize(num_local_particles);

    octree_nodes_diameters_inv2.resize(num_levels);
}

void Octree::computeLocalAABB(float local_min[3], float local_max[3])
{
    std::fill_n(local_min,3,std::numeric_limits<float>::max());
    std::fill_n(local_max,3,-std::numeric_limits<float>::max());

    //#pragma omp parallel
    {
        reduceMinMax(particles_storage_local[POS_X].data(),num_local_particles,local_min[0],local_max[0]);
        reduceMinMax(particles_storage_local[POS_Y].data(),num_local_particles,local_min[1],local_max[1]);
        reduceMinMax(particles_storage_local[POS_Z].data(),num_local_particles,local_min[2],local_max[2]);
    }

}

void Octree::computeGlobalAABB(float local_min[3], float local_max[3], float center[3], float& diameter)
{
    float global_min[3];
    float global_max[3];

    std::fill_n(global_min,3,std::numeric_limits<float>::max());
    std::fill_n(global_max,3,-std::numeric_limits<float>::max());

    MPI_Allreduce(local_min,global_min,3,MPI_FLOAT,MPI_MIN,MPI_COMM_WORLD);
    MPI_Allreduce(local_max,global_max,3,MPI_FLOAT,MPI_MAX,MPI_COMM_WORLD);

    for(int i=0; i<3; ++i)
        center[i] = global_min[i] + (global_max[i] - global_min[i])/2;

    diameter = std::max(
                std::max(global_max[2]-global_min[2],global_max[1]-global_min[1]),
                global_max[0]-global_min[0]);
}

void Octree::computeLocalSFCKeys(float center[], float diameter)
{
    float* posx = particles_storage_local[POS_X].data();
    float* posy = particles_storage_local[POS_Y].data();
    float* posz = particles_storage_local[POS_Z].data();
    uint32_t* key = sfc_keys_local.data();

    ASSUME_DEFAULT_ALIGNMENT(posx);
    ASSUME_DEFAULT_ALIGNMENT(posy);
    ASSUME_DEFAULT_ALIGNMENT(posz);
    ASSUME_DEFAULT_ALIGNMENT(key);

    const float scale_factor = octree_dim/(diameter+0.0001);

    // we need positive values for sfc key alg
    float radius = diameter / 2;
    const float offset[3] = {
        (0 - (center[0] - radius))*scale_factor,
        (0 - (center[1] - radius))*scale_factor,
        (0 - (center[2] - radius))*scale_factor
    };

    // (posx[i]+(offset_without_scale[0]))*scale_factor  =
    // = posx[i]*scale_factor + offset_with_scale[0]

    const unsigned int n_local_p = num_local_particles;

    #pragma omp simd aligned(posx,posy,posz,key:DEFAULT_ALIGNMENT) safelen(8)
    for(unsigned int i=0; i<n_local_p; ++i)
    {
        // move point by offset and scale
        // offset si already scaled
        float px = posx[i]*scale_factor + offset[0];
        float py = posy[i]*scale_factor + offset[1];
        float pz = posz[i]*scale_factor + offset[2];

        key[i] = get_key_morton(static_cast<uint32_t>(px),
                                static_cast<uint32_t>(py),
                                static_cast<uint32_t>(pz));
    }
}

void Octree::buildLocalSortedSFCIndices()
{
    const int index_offset = my_rank*num_local_particles;

    std::iota(sort_helper_indices.begin(),sort_helper_indices.end(),0);

    NBodySFCComparator<uint32_t> comp(sfc_keys_local);
    std::sort(sort_helper_indices.begin(),sort_helper_indices.end(),comp);

    std::for_each(sort_helper_indices.begin(),sort_helper_indices.end(), [index_offset](uint32_t& d) { d+=index_offset;});
}

void Octree::rearrangeLocalParticles()
{
    if(comm_node)
    {
        MPI_Waitall(6,request_gatherposvel,MPI_STATUSES_IGNORE);
    }

    //#pragma omp parallel
    {
        for(int pa = PARTICLE_ATTRIB_FIRST; pa < PARTICLE_ATTRIBS_MAX; ++pa)
        {
            float* localp = particles_storage_local[pa].data();
            float* globalp = particles_storage_global[pa].data();

            ASSUME_DEFAULT_ALIGNMENT(localp);
            ASSUME_DEFAULT_ALIGNMENT(globalp);

            #pragma omp simd
            for(int i=0; i<num_local_particles; ++i)
            {
                localp[i] = globalp[sort_helper_indices[i]];
            }
        }

        for(int i=0; i<num_local_particles; ++i)
        {
            sfc_keys_local[i] = sfc_keys_global[sort_helper_indices[i]];
        }
    }
}

void Octree::buildLocalOctree()
{
    // compute particles counts in leaf nodes
    uint32_t* histogram = nodes_particle_counts[num_levels-1].data();

    int tid = omp_get_thread_num();
    int num_threads = omp_get_num_threads();

    uint32_t* hp = histogram_private[tid].data();

    std::fill_n(hp,histogram_size,0);

    for(int i=0; i<num_local_particles; ++i)
    {
        ++hp[sfc_keys_local[i]];
    }

    for(int i=0; i<histogram_size; ++i)
    {
        int s = 0;

        for(int t=0; t<num_threads; ++t)
        {
            s += histogram_private[t][i];
        }

        histogram[i] = s;
    }

    // parciles counts in rest of nodes
    for(int level = num_levels-2; level >= 0; --level)
    {
        const int num_nodes_on_this_level = std::pow(8,level);

        for(int i=0; i<num_nodes_on_this_level; ++i)
        {
            uint32_t children_offset = i*8;

            uint32_t* children_data = nodes_particle_counts[level+1].data() + children_offset;

            ASSUME_ALIGNED(children_data,32);

            uint32_t sum = 0;

            #pragma omp simd reduction(+:sum)
            for(int j=0; j<8; ++j)
            {
                sum += children_data[j];
            }

            nodes_particle_counts[level][i] = sum;
        }
    }

    // build indices in leaf nodes to particle storage
    size_t my_offset = my_rank * num_local_particles;

    std::fill_n(nodes_groups_indices.begin(),histogram_size,-1);    // to construct global version of this, we jsut do mpi reduce with min operator

    // fill nodes_groups_indices

    uint32_t pos = my_offset;
    uint32_t count_prev = 0;

    for( int sfc_key=0; sfc_key<histogram_size; ++sfc_key)
    {
        auto count = nodes_particle_counts[num_levels-1][sfc_key];
        nodes_groups_indices[sfc_key] = -1;

        if(count != 0)
        {
            pos += count_prev;
            nodes_groups_indices[sfc_key] = pos;

            count_prev = count;
        }
    }

    // build last octree level - compute center of mass and total mass in leaf nodes
    for( int sfc_key=0; sfc_key<histogram_size; ++sfc_key)
    {
        auto count = nodes_particle_counts[num_levels-1][sfc_key];

        float mass = 0.0f;
        float pos_x = 0.0f;
        float pos_y = 0.0f;
        float pos_z = 0.0f;

        if(count > 0)
        {
            const uint32_t base = nodes_groups_indices[sfc_key] - my_offset;
            for(uint32_t i=0; i<count; ++i)
            {
                const float my_mass = particles_storage_local[MASS][base+i];
                mass += my_mass;
                pos_x += particles_storage_local[POS_X][base+i] * my_mass;
                pos_y += particles_storage_local[POS_Y][base+i] * my_mass;
                pos_z += particles_storage_local[POS_Z][base+i] * my_mass;
            }

            pos_x /= mass;
            pos_y /= mass;
            pos_z /= mass;
        }

        nodes[num_levels-1][MASS][sfc_key] = mass;
        nodes[num_levels-1][POS_X][sfc_key] = pos_x;
        nodes[num_levels-1][POS_Y][sfc_key] = pos_y;
        nodes[num_levels-1][POS_Z][sfc_key] = pos_z;
    }

    // build rest of octree ( bottom to top ) - compute center of mass and total mass
    for(int level = num_levels-2; level >= 0; --level)
    {
        const int num_nodes_on_this_level = std::pow(8,level);

        float* nx = nodes[level+1][POS_X].data();
        float* ny = nodes[level+1][POS_Y].data();
        float* nz = nodes[level+1][POS_Z].data();
        float* nm = nodes[level+1][MASS].data();

        ASSUME_ALIGNED(nx,ALIGNMENT);
        ASSUME_ALIGNED(ny,ALIGNMENT);
        ASSUME_ALIGNED(nz,ALIGNMENT);
        ASSUME_ALIGNED(nm,ALIGNMENT);

        //#pragma omp parallel for if( num_nodes_on_this_level > 1 )
        #pragma omp simd
        for(int i=0; i<num_nodes_on_this_level; ++i)
        {
            uint32_t children_offset = i*8;

            float mass = 0.0f;
            float pos_x = 0.0f;
            float pos_y = 0.0f;
            float pos_z = 0.0f;

            for(int j=0; j<8; ++j)
            {
                const float my_mass = nm[children_offset+j];
                mass += my_mass;
                pos_x += nx[children_offset+j] * my_mass;
                pos_y += ny[children_offset+j] * my_mass;
                pos_z += nz[children_offset+j] * my_mass;
            }

            if(mass != 0.0f)
            {
                pos_x /= mass;
                pos_y /= mass;
                pos_z /= mass;
            }

            nodes[level][MASS][i] = mass;
            nodes[level][POS_X][i] = pos_x;
            nodes[level][POS_Y][i] = pos_y;
            nodes[level][POS_Z][i] = pos_z;
        }
    }
}

#pragma omp declare simd
float vectorLength2(float x, float y, float z)
{
    return (x*x + y*y + z*z);
}

//#pragma omp declare simd uniform(level,node) linear(particle)
void Octree::traverse(float& fx, float& fy, float& fz, uint8_t level, uint32_t node,uint32_t particle)
{
    if(nodes_particle_counts[level][node] == 0)
    {
        return;
    }

    const float inv_theta2 = this->inv_theta2;
    const float eps2 = softening2;

    //center of mass
    float cmx = nodes[level][POS_X][node];
    float cmy = nodes[level][POS_Y][node];
    float cmz = nodes[level][POS_Z][node];

    // distance - particle x center of mass
    float rx = cmx - particles_storage_local[POS_X][particle];
    float ry = cmy - particles_storage_local[POS_Y][particle];
    float rz = cmz - particles_storage_local[POS_Z][particle];

    float len2 = vectorLength2(rx,ry,rz);

    // d = distance
    // s = octree node edge len
    // continue_traversal = s/d > theta
    //                      s/d > theta         / *d
    //                      s   > theta*d       / *(1/s)  *(1/theta)
    //                  1/theta > d/s
    //  1/theta >= 0
    //  d >= 0
    //  s > 0
    //  d/s > 0
    //                  1/theta > d/s           / ^2
    //                1/theta^2 > d^2 * (1/s^2)
    // continue_traversal = inv_theta2 > len2 * octree_nodes_diameters_inv2[level]

    bool continue_traversal = ((len2 * octree_nodes_diameters_inv2[level]) < inv_theta2);

    if(!continue_traversal)
    {
#ifdef COLLECT_TRAVERSE_STATS
        ++traversal_stats[level];  // FIXME: parallel ver
#endif

        len2 += eps2;
        float dist6 = len2 * len2 * len2;
        float invDistCube = 1.0f/sqrt(dist6);

        float mass = nodes[level][MASS][node];

        fx += mass * invDistCube * rx;
        fy += mass * invDistCube * ry;
        fz += mass * invDistCube * rz;
    }
    else
    {
        if(level!=num_levels-1)
        {
            for(int n=0; n<8; ++n)
                traverse(fx,fy,fz,level+1,8*node+n,particle);
        }
        else
        {
            // TODO: collect only index to groups where we need to descend and calculate forces after traversal

            uint32_t particles_count = nodes_particle_counts[num_levels-1][node];

            if(particles_count == 0)
            {
                return;
            }

            uint32_t particles_offset = nodes_groups_indices[node];

            for(int i=0; i<particles_count; ++i)
            {
#ifdef COLLECT_TRAVERSE_STATS
                    ++traversal_stats[level];
#endif

                float cmx = particles_storage_global[POS_X][particles_offset+i];
                float cmy = particles_storage_global[POS_Y][particles_offset+i];
                float cmz = particles_storage_global[POS_Z][particles_offset+i];

                float rx = cmx - particles_storage_local[POS_X][particle];
                float ry = cmy - particles_storage_local[POS_Y][particle];
                float rz = cmz - particles_storage_local[POS_Z][particle];

                float len2 = vectorLength2(rx,ry,rz);

                len2 += eps2;
                float dist6 = len2 * len2 * len2;
                float invDistCube = 1.0f/sqrt(dist6);

                float mass = particles_storage_global[MASS][particles_offset+i];

                fx += mass * invDistCube * rx;
                fy += mass * invDistCube * ry;
                fz += mass * invDistCube * rz;
            }
        }
    }
}

void Octree::computeForces()
{
    for(int i=0; i<num_local_particles; ++i)
    {
        float fx = 0.0f;
        float fy = 0.0f;
        float fz = 0.0f;

        traverse(fx,fy,fz,0,0,i);

        local_forces[0][i] = fx;
        local_forces[1][i] = fy;
        local_forces[2][i] = fz;
    }
}

void Octree::computeForces2()
{
    const int block_size = 64;

    std::fill_n(traverse_list[2].begin(),traverse_list[2].size(),1);

    for(int i=3; i<num_levels-1; ++i)
        std::fill_n(traverse_list[i].begin(),traverse_list[1].size(),0);

    const float inv_theta2 = this->inv_theta2;
    const float eps2 = softening2;

    const int last_level = num_levels-1;
    char* lltrav = traverse_list[last_level].data();

    ASSUME_DEFAULT_ALIGNMENT(lltrav);

    float* posx_g = particles_storage_global[POS_X].data();
    float* posy_g = particles_storage_global[POS_Y].data();
    float* posz_g = particles_storage_global[POS_Z].data();
    float* mass_g = particles_storage_global[MASS].data();
    float* posx_l = particles_storage_local[POS_X].data();
    float* posy_l = particles_storage_local[POS_Y].data();
    float* posz_l = particles_storage_local[POS_Z].data();
    float* nx = nodes[last_level][POS_X].data();
    float* ny = nodes[last_level][POS_Y].data();
    float* nz = nodes[last_level][POS_Z].data();
    float* nm = nodes[last_level][MASS].data();

    uint32_t* ngi = nodes_groups_indices.data();
    uint32_t* llpcounts = nodes_particle_counts[last_level].data();

    ASSUME_DEFAULT_ALIGNMENT(posx_g);
    ASSUME_DEFAULT_ALIGNMENT(posy_g);
    ASSUME_DEFAULT_ALIGNMENT(posz_g);
    ASSUME_DEFAULT_ALIGNMENT(posx_l);
    ASSUME_DEFAULT_ALIGNMENT(posy_l);
    ASSUME_DEFAULT_ALIGNMENT(posz_l);
    ASSUME_DEFAULT_ALIGNMENT(mass_g);
    ASSUME_DEFAULT_ALIGNMENT(nx);
    ASSUME_DEFAULT_ALIGNMENT(ny);
    ASSUME_DEFAULT_ALIGNMENT(nz);
    ASSUME_DEFAULT_ALIGNMENT(nm);
    ASSUME_DEFAULT_ALIGNMENT(ngi);
    ASSUME_DEFAULT_ALIGNMENT(llpcounts);

    if(comm_node)
        MPI_Waitall(7,request_gatherparticles,MPI_STATUSES_IGNORE);

    #pragma omp parallel for
    for(int particle=0; particle<num_local_particles; ++particle)
    {
        float fx = 0.0f;
        float fy = 0.0f;
        float fz = 0.0f;

        for(int level=2; level<num_levels-1; ++level)
        {
            for(int t=0; t<traverse_list[level].size(); ++t)
            {
                if(traverse_list[level][t])
                {
                    for(int n=0; n<8; ++n)
                    {
                        int node = getFirstChild(t)+n;

                        if(nodes_particle_counts[level+1][node] != 0)
                        {
                            //center of mass
                            float cmx = nodes[level+1][POS_X][node];
                            float cmy = nodes[level+1][POS_Y][node];
                            float cmz = nodes[level+1][POS_Z][node];

                            // distance - particle x center of mass
                            float rx = cmx - posx_l[particle];
                            float ry = cmy - posy_l[particle];
                            float rz = cmz - posz_l[particle];

                            float len2 = vectorLength2(rx,ry,rz);

                            bool continue_traversal = ((len2 * octree_nodes_diameters_inv2[level]) < inv_theta2);

                            traverse_list[level+1][node] = continue_traversal;

                            if(!continue_traversal)
                            {
                                len2 += eps2;
                                float dist6 = len2 * len2 * len2;
                                float invDistCube = 1.0f/sqrt(dist6);

                                float mass = nodes[level+1][MASS][node];

                                fx += mass * invDistCube * rx;
                                fy += mass * invDistCube * ry;
                                fz += mass * invDistCube * rz;
                            }
                        }
                    }
                }
            }
        }

        for(int node=0; node<traverse_list[num_levels-1].size(); ++node)
        {
            if(lltrav[node])
            {
                //center of mass
                float cmx = nx[node];
                float cmy = ny[node];
                float cmz = nz[node];

                // distance - particle x center of mass
                float rx = cmx - posx_l[particle];
                float ry = cmy - posy_l[particle];
                float rz = cmz - posz_l[particle];

                float len2 = vectorLength2(rx,ry,rz);

                bool continue_traversal = ((len2 * octree_nodes_diameters_inv2[last_level]) < inv_theta2);

                if(!continue_traversal)
                {
                    len2 += eps2;
                    float dist6 = len2 * len2 * len2;
                    float invDistCube = 1.0f/sqrt(dist6);

                    float mass = nm[node];

                    fx += mass * invDistCube * rx;
                    fy += mass * invDistCube * ry;
                    fz += mass * invDistCube * rz;
                }
                else
                {
                    // calculate particle-particle forces
                    uint32_t particles_offset = ngi[node];
                    uint32_t particles_count = llpcounts[node];

                    for(int i=0; i<particles_count; ++i)
                    {
                        float cmx = posx_g[particles_offset+i];
                        float cmy = posy_g[particles_offset+i];
                        float cmz = posz_g[particles_offset+i];

                        float rx = cmx - posx_l[particle];
                        float ry = cmy - posy_l[particle];
                        float rz = cmz - posz_l[particle];

                        float len2 = vectorLength2(rx,ry,rz);

                        len2 += eps2;
                        float dist6 = len2 * len2 * len2;
                        float invDistCube = 1.0f/sqrt(dist6);

                        float mass = mass_g[particles_offset+i];

                        fx += mass * invDistCube * rx;
                        fy += mass * invDistCube * ry;
                        fz += mass * invDistCube * rz;
                    }
                }
            }
        }

        local_forces[0][particle] = fx;
        local_forces[1][particle] = fy;
        local_forces[2][particle] = fz;
    }
}

void Octree::updateLocalVelPos(float dtime)
{
    float* fx = local_forces[0].data();
    float* fy = local_forces[1].data();
    float* fz = local_forces[2].data();

    float* vx = particles_storage_local[VEL_X].data();
    float* vy = particles_storage_local[VEL_Y].data();
    float* vz = particles_storage_local[VEL_Z].data();

    float* px = particles_storage_local[POS_X].data();
    float* py = particles_storage_local[POS_Y].data();
    float* pz = particles_storage_local[POS_Z].data();

    ASSUME_DEFAULT_ALIGNMENT(fx);
    ASSUME_DEFAULT_ALIGNMENT(fy);
    ASSUME_DEFAULT_ALIGNMENT(fz);
    ASSUME_DEFAULT_ALIGNMENT(vx);
    ASSUME_DEFAULT_ALIGNMENT(vy);
    ASSUME_DEFAULT_ALIGNMENT(vz);
    ASSUME_DEFAULT_ALIGNMENT(px);
    ASSUME_DEFAULT_ALIGNMENT(py);
    ASSUME_DEFAULT_ALIGNMENT(pz);

    #pragma omp simd
    for (int i = 0; i < num_local_particles; ++i)
    {
        vx[i] += fx[i] * G * dtime;
        vy[i] += fy[i] * G * dtime;
        vz[i] += fz[i] * G * dtime;

        px[i] += vx[i] * dtime;
        py[i] += vy[i] * dtime;
        pz[i] += vz[i] * dtime;
    }
}

void Octree::mpiScatterParticles()
{
    for(int i=MASS; i<=VEL_Z; ++i)
    {
        MPI_Scatter(particles_storage_global[i].data(),num_local_particles,MPI_FLOAT,
                    particles_storage_local[i].data(),num_local_particles,MPI_FLOAT,
                    0,MPI_COMM_WORLD);
    }
}

void Octree::mpiBroadcastParticles()
{
    for(int i=PARTICLE_ATTRIB_FIRST; i<PARTICLE_ATTRIBS_MAX; ++i)
    {
        MPI_Bcast(particles_storage_global[i].data(),num_global_particles,MPI_FLOAT,
                  0,MPI_COMM_WORLD);
    }
}

void Octree::mpiAllGatherSFCKeys()
{
    MPI_Allgather(sfc_keys_local.data(),num_local_particles,MPI_UNSIGNED,
                  sfc_keys_global.data(),num_local_particles,MPI_UNSIGNED,
                  MPI_COMM_WORLD);
}

void Octree::mpiAllGatherParticles()
{
    if(!comm_node)
    {
        for(int i=PARTICLE_ATTRIB_FIRST; i<PARTICLE_ATTRIBS_MAX; ++i)
        {
            MPI_Allgather(particles_storage_local[i].data(),num_local_particles,MPI_FLOAT,
                          particles_storage_global[i].data(),num_local_particles,MPI_FLOAT,
                          MPI_COMM_WORLD);
        }
    }
    else
    {
        for(int i=PARTICLE_ATTRIB_FIRST; i<PARTICLE_ATTRIBS_MAX; ++i)
        {
            MPI_Iallgather(particles_storage_local[i].data(),num_local_particles,MPI_FLOAT,
                          particles_storage_global[i].data(),num_local_particles,MPI_FLOAT,
                          MPI_COMM_WORLD,request_gatherparticles+(i-PARTICLE_ATTRIB_FIRST));
        }
    }


    //TODO: potrebujeme?
//    MPI_Allgather(sfc_keys_local.data(),num_local_particles,MPI_UNSIGNED,
//                  sfc_keys_global.data(),num_local_particles,MPI_UNSIGNED,
//                  MPI_COMM_WORLD);
}

void Octree::mpiAllGatherPosVel()
{
    if(!comm_node)
    {
        for(int i=POS_X; i<PARTICLE_ATTRIBS_MAX; ++i)
        {
            MPI_Allgather(particles_storage_local[i].data(),num_local_particles,MPI_FLOAT,
                          particles_storage_global[i].data(),num_local_particles,MPI_FLOAT,
                          MPI_COMM_WORLD);
        }
    }
    else
    {
        for(int i=POS_X; i<PARTICLE_ATTRIBS_MAX; ++i)
        {
            MPI_Iallgather(particles_storage_local[i].data(),num_local_particles,MPI_FLOAT,
                           particles_storage_global[i].data(),num_local_particles,MPI_FLOAT,
                           MPI_COMM_WORLD,request_gatherposvel+(i-POS_X));
        }
    }
}

void Octree::mpiSortSFCIndices()
{
    NBodySFCComparator<uint32_t> comp(sfc_keys_global);

    const int left = my_rank - 1;
    const int right = my_rank + 1;

    const int target1 = my_rank%2 == 0 ? right : left;
    const int target2 = my_rank%2 == 0 ? left : right;

    const int assing_begin_offset1 = my_rank%2 == 0 ? 0 : num_local_particles;
    const int assing_begin_offset2 = my_rank%2 == 0 ? num_local_particles : 0;
    const int assing_end_offset1 = my_rank%2 == 0 ? num_local_particles : 2*num_local_particles;
    const int assing_end_offset2 = my_rank%2 == 0 ? 2*num_local_particles : num_local_particles;

    const int n_steps = world_size/2.0f+0.5f;

    for(int step = 0; step < n_steps; ++step)
    {
        if(target1>=0 && target1 < world_size)
        {
            MPI_Sendrecv(sort_helper_indices.data(),num_local_particles,MPI_UNSIGNED,target1,step,
                         mergeSplitRecvBuffer.data(),num_local_particles,MPI_UNSIGNED,target1,step,
                         MPI_COMM_WORLD,MPI_STATUS_IGNORE);

            // aby susedia volali std::merge s parametrami v rovnakom poradi.
            // bez first a second sa moze stat, ak hranicne castice maju rovnaky SFC
            // tak sort_helper_indices budu v nerovnake v susednych procesoch
            auto& first = my_rank%2 == 0 ? sort_helper_indices : mergeSplitRecvBuffer;
            auto& second = my_rank%2 == 0 ? mergeSplitRecvBuffer : sort_helper_indices;

            std::merge(first.begin(),first.end(),
                       second.begin(),second.end(),
                       mergeSplitBuffer.begin(),comp);

            sort_helper_indices.assign(mergeSplitBuffer.begin()+assing_begin_offset1,
                                       mergeSplitBuffer.begin()+assing_end_offset1);

        }

        if(target2>=0 && target2 < world_size)
        {
            MPI_Sendrecv(sort_helper_indices.data(),num_local_particles,MPI_UNSIGNED,target2,step,
                         mergeSplitRecvBuffer.data(),num_local_particles,MPI_UNSIGNED,target2,step,
                         MPI_COMM_WORLD,MPI_STATUS_IGNORE);

            auto& first = my_rank%2 == 0 ? sort_helper_indices : mergeSplitRecvBuffer;
            auto& second = my_rank%2 == 0 ? mergeSplitRecvBuffer : sort_helper_indices;

            std::merge(first.begin(),first.end(),
                       second.begin(),second.end(),
                       mergeSplitBuffer.begin(),comp);

            sort_helper_indices.assign(mergeSplitBuffer.begin()+assing_begin_offset2,
                                       mergeSplitBuffer.begin()+assing_end_offset2);
        }

        MPI_Barrier(MPI_COMM_WORLD);
    }
}

void Octree::mpiBuildGlobalOctree()
{
    // premultiply positions with mass in local octree nodes
    for(int level=0; level<num_levels; ++level)
    {
        int num_nodes_on_this_level = std::pow(8,level);

        for(int i=0; i<num_nodes_on_this_level; ++i)
        {
            float mass = nodes[level][MASS][i];
            nodes[level][POS_X][i] *= mass;
            nodes[level][POS_Y][i] *= mass;
            nodes[level][POS_Z][i] *= mass;
        }
    }

    // collect mass
    for(int level=0; level<num_levels; ++level)
    {
        MPI_Allreduce(MPI_IN_PLACE,nodes[level][MASS].data(),nodes[level][MASS].size(),MPI_FLOAT,
                      MPI_SUM,MPI_COMM_WORLD);
    }

    // build nodes_groups_indices
    MPI_Allreduce(MPI_IN_PLACE,nodes_groups_indices.data(),nodes_groups_indices.size(),
                  MPI_UNSIGNED,MPI_MIN,MPI_COMM_WORLD);


    // compute total mass @ nodes
    for(int level=0; level<num_levels; ++level)
    {
        MPI_Allreduce(MPI_IN_PLACE,nodes_particle_counts[level].data(),nodes_particle_counts[level].size(),MPI_UNSIGNED,
                      MPI_SUM,MPI_COMM_WORLD);
    }

    // sum reduce positions
    for(int level=0; level<num_levels; ++level)
    {
        MPI_Allreduce(MPI_IN_PLACE,nodes[level][POS_X].data(),nodes[level][POS_X].size(),MPI_FLOAT,
                      MPI_SUM,MPI_COMM_WORLD);
        MPI_Allreduce(MPI_IN_PLACE,nodes[level][POS_Y].data(),nodes[level][POS_Y].size(),MPI_FLOAT,
                      MPI_SUM,MPI_COMM_WORLD);
        MPI_Allreduce(MPI_IN_PLACE,nodes[level][POS_Z].data(),nodes[level][POS_Z].size(),MPI_FLOAT,
                      MPI_SUM,MPI_COMM_WORLD);
    }

    // TODO: MPI_Reduce_scatter, multiply, MPI_Allgather

    // compute final positions
    for(int level=0; level<num_levels; ++level)
    {
        int num_nodes_on_this_level = std::pow(8,level);

        for(int i=0; i<num_nodes_on_this_level; ++i)
        {
            float mass = nodes_particle_counts[level][i] != 0 ? 1/nodes[level][MASS][i] : 0;
            nodes[level][POS_X][i] *= mass;
            nodes[level][POS_Y][i] *= mass;
            nodes[level][POS_Z][i] *= mass;
        }
    }

}

uint32_t Octree::generateRandomParticles(uint32_t n, float weight_min, float weight_max, float world_size)
{
#ifndef __MIC__
    num_global_particles = n;

    std::default_random_engine gen;
    //uniform_real_distribution<float> dist(-world_size,world_size);
    //normal_distribution<float> dist(0, world_size/4);
    std::uniform_real_distribution<float> dist(-world_size/2,world_size/2);

    auto generator = std::bind(dist,std::ref(gen));

    //for(avector<float>& av : particles)
    for(int i = PARTICLE_ATTRIB_FIRST; i < PARTICLE_ATTRIBS_MAX; ++i)
    {
        AlignedVector<float>& av = particles_storage_global[i];
        av.clear();
        av.reserve(n);
        if(i==MASS)
        {
            fill_n(back_inserter(av),n,weight_max);
        }
        else
            generate_n(back_inserter(av),n,generator);
    }

    for(int i=VEL_X; i<=VEL_Z; ++i)
    {
        AlignedVector<float>& av = particles_storage_global[i];

        std::transform(av.begin(),av.end(),av.begin(),
                       [](float v) { return v / 20; });
    }

    return n;
#endif
}

uint32_t Octree::loadParticles(const char *filename)
{
    ifstream ifs(filename);

    ifs.exceptions();

    if (!ifs.is_open())
    {
        throw runtime_error(std::string("Can't open file ") + filename);
    }

    num_global_particles = 0;
    ifs >> num_global_particles;

    for(AlignedVector<float>& av : particles_storage_global)
    {
        av.clear();
        av.reserve(num_global_particles);
        copy_n(std::istream_iterator<float>(ifs),num_global_particles,std::back_inserter(av));
    }

    ifs.close();

    return num_global_particles;
}

void Octree::saveParticles(const char *filename)
{
    ofstream ofs(filename);

    if(!ofs.is_open())
    {
        throw runtime_error(std::string("Can't open file ") + filename);
    }

    ofs << num_global_particles << "\n";

    for(AlignedVector<float>& av : particles_storage_global)
    {
        std::copy(av.begin(),av.end(),std::ostream_iterator<float>(ofs," "));
        ofs << '\n';
    }

    //std::copy(sfc_keys_global.begin(),sfc_keys_global.end(),std::ostream_iterator<uint32_t>(ofs," "));

    ofs.close();
}

void Octree::updateOctreeNodeDimensions(float diameter)
{
    for(int i=0; i<num_levels; ++i)
    {
        octree_nodes_diameters_inv2[i] = std::pow((1/(diameter/std::pow(2,i))),2.0f);
    }
}

void Octree::printLocalParticles()
{
    cout << num_local_particles << "\n";

    for(AlignedVector<float>& av : particles_storage_local)
    {
        std::copy(av.begin(),av.end(),std::ostream_iterator<float>(cout," "));
        cout << '\n';
    }

    //std::copy(sfc_keys_global.begin(),sfc_keys_global.end(),std::ostream_iterator<uint32_t>(ofs," "));

}

void Octree::printGlobalParticles()
{
    cout << num_global_particles << "\n";

    for(AlignedVector<float>& av : particles_storage_global)
    {
        std::copy(av.begin(),av.end(),std::ostream_iterator<float>(cout," "));
        cout << '\n';
    }

    //std::copy(sfc_keys_global.begin(),sfc_keys_global.end(),std::ostream_iterator<uint32_t>(ofs," "));
}

float Octree::calculateChecksum()
{
    float sum;
    for(AlignedVector<float>& av : particles_storage_global)
    {
        sum = std::accumulate(av.begin(),av.end(),sum,
                              [](const float p1, const float p2) -> float
                              {
                                  return std::abs(p1) + std::abs(p2);
                              });
    }

    return sum;
}

AlignedVector<uint64_t> &Octree::getTraversalStats()
{
    static bool done = false;
    if(!done)
    {
        MPI_Allreduce(MPI_IN_PLACE,traversal_stats.data(),traversal_stats.size(),MPI_UNSIGNED_LONG,
                      MPI_SUM,MPI_COMM_WORLD);
        done = true;
    }

    return traversal_stats;
}

void Octree::dumpState()
{
    cout << "Nodes" << endl;
    for( auto& a : nodes )
    {
        for(int i=0; i<PARTICLE_ATTRIB_OCTREE_NODE_ATTRIBS_MAX; ++i)
        {
            std::copy(a[i].begin(),a[i].end(),std::ostream_iterator<float>(cout," "));
            cout << endl;
        }
    }

    cout << "Particle counts" << endl;
    for( auto& a : nodes_particle_counts)
    {
        std::copy(a.begin(),a.end(),std::ostream_iterator<uint32_t>(cout," "));
        cout << endl;
    }

    cout << "nodes groups indices" << endl;
    std::copy(nodes_groups_indices.begin(),nodes_groups_indices.end(),std::ostream_iterator<uint32_t>(cout," "));
    cout << endl;

}
